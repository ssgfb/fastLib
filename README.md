### 不再维护更新
因为初期设计就是以兴趣和学习启动的项目，目前很多功能已过时，也不准备重构了，所以关闭项目

### 项目包说明

+ adapter       一些抽象适配器
+ alpha         开发中功能，不建议直接使用
+ anim          动画包（当前仅有ViewPage切换动画）
+ annotation    整个项目中的自定义注解
+ app           核心功能 如封装的Activity，封装Dialog，全局广播
+ aspect        切面相关功能
+ base          基础易扩展模块
+ bean          事件和其他实体类 一般用户除了事件类，不使用此包中其他类
+ db            数据库封装模块 此封装的数据库是非orm的
+ net           网络封装模块,底层使用socket实现
+ utils         非常通用的工具类包
+ widget        基本上都是继承View或者相关的功能模块

### 第三方依赖
+ Gson  (json解析)
+ androidx  (兼容包)
+ google material   (material样式支持)
+ cglib-for-android (动态生成类,切面支持)

### 导入使用

Gradle:

    dependencies{
        implementation 'com.sgfb:fastlib:1.1.5'
    }


[jar下载](https://gitee.com/ssgfb/fastLib/releases)

博客教程 http://blog.csdn.net/sb602687446