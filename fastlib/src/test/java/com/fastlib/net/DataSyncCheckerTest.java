package com.fastlib.net;

import com.fastlib.db.condition_utils.ChangedData;
import com.fastlib.db.condition_utils.DataSyncChecker;
import com.fastlib.db.condition_utils.ExternalLoader;
import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by sgfb on 2020\03\15.
 */
public class DataSyncCheckerTest {
    DataSyncChecker<String> mChecker;

    @Before
    public void setUp() throws Exception {
        mChecker=new DataSyncChecker<>(() -> {
            List<String> initList=new ArrayList<>();
            initList.add("1");
            initList.add("2");
            initList.add("3");
            return initList;
        });
    }

    @Test
    public void testIncrease(){
        List<String> expect=new ArrayList<>();
        expect.add("1");
        expect.add("2");
        expect.add("3");

        mChecker.add("1");
        assertEquals(expect, mChecker.getInternalDataList());

        mChecker.add("4");
        expect.add("4");
        assertEquals(expect,mChecker.getInternalDataList());
    }

    @Test
    public void testDecrease(){
        List<String> expect=new ArrayList<>();
        expect.add("1");
        expect.add("2");
        expect.add("3");

        mChecker.delete("4");
        assertEquals(expect, mChecker.getInternalDataList());

        mChecker.delete("1");
        expect.set(0,null);
        assertEquals(expect, mChecker.getInternalDataList());
    }

    @Test
    public void testSyncData(){
        mChecker.delete("3");
        mChecker.add("4");
        mChecker.valueChanged("1","5");

        List<ChangedData<String>> real=mChecker.syncData();
        List<ChangedData<String>> expect=new ArrayList<>();
        expect.add(new ChangedData<>(0, ChangedData.TYPE_VALUE_CHANGED,"5"));
        expect.add(new ChangedData<>(2, ChangedData.TYPE_DELETE,"3"));
        expect.add(new ChangedData<>(3, ChangedData.TYPE_ADD,"4"));
        assertEquals(expect.size(),real.size());

        for(int i=0;i<expect.size();i++){
            assertTrue(expect.get(i).equals(real.get(i)));
        }
    }
}