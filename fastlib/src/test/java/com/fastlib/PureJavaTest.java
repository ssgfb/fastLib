package com.fastlib;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayInputStream;

/**
 * Created by sgfb on 2020\04\03.
 */
@RunWith(JUnit4.class)
public class PureJavaTest{

    @Test
    public void testInputStream(){
        ByteArrayInputStream bais=new ByteArrayInputStream("abc".getBytes());
        System.out.println(bais.available());

        bais.read();
        System.out.println(bais.available());
    }
}
