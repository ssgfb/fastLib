package com.fastlib.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.Nullable;

import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fastlib.R;

/**
 * Created by sgfb on 2018/6/12.
 * 左右两端进度滑动条
 */
public class DoubleSeekBar extends View {
    private final int RADIUS = 15;

    private boolean isSelectLeft; //or right
    private int mMin = 0;
    private int mMax = 100;
    private float mLeftCirclePosition;
    private float mRightCirclePosition;
    private Paint mLeftCirclePaint;
    private Paint mRightCirclePaint;
    private Paint mProgressPaint;
    private Paint mBackgroundLinePaint;
    private OnProgressChanged mListener;
    private int mCurrLeftValue = 0;
    private int mCurrRightValue = 50;

    {
        mBackgroundLinePaint = new Paint();
        mBackgroundLinePaint.setColor(Color.WHITE);
        mBackgroundLinePaint.setStyle(Paint.Style.FILL);

        mLeftCirclePaint = new Paint();
        mLeftCirclePaint.setColor(Color.WHITE);
        mLeftCirclePaint.setStyle(Paint.Style.FILL);
        mLeftCirclePaint.setAntiAlias(true);

        mRightCirclePaint = new Paint();
        mRightCirclePaint.setColor(Color.WHITE);
        mRightCirclePaint.setStyle(Paint.Style.FILL);
        mRightCirclePaint.setAntiAlias(true);

        mProgressPaint = new Paint();
        mProgressPaint.setColor(Color.GREEN);
        mProgressPaint.setStyle(Paint.Style.FILL);
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        isSelectLeft = Math.abs(mLeftCirclePosition - event.getX()) < Math.abs(mRightCirclePosition - event.getX());
                        translationCircle(event.getX());
                        break;
                    case MotionEvent.ACTION_MOVE:
                        translationCircle(event.getX());
                        break;
                }
                return true;
            }
        });
    }

    private void translationCircle(float newX) {
        if (isSelectLeft) {
            mLeftCirclePosition = newX;
            if (mLeftCirclePosition > mRightCirclePosition)
                mLeftCirclePosition = mRightCirclePosition;
        } else {
            mRightCirclePosition = newX;
            if (mRightCirclePosition < mLeftCirclePosition)
                mRightCirclePosition = mLeftCirclePosition;
        }
        invalidate();
    }

    public DoubleSeekBar(Context context) {
        super(context);
    }

    public DoubleSeekBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DoubleSeekBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //如果高度是wrap_content则设置个最小值
        if(MeasureSpec.getMode(heightMeasureSpec)==MeasureSpec.AT_MOST){
            int realHeight=RADIUS*2;
            realHeight+=getPaddingTop();
            realHeight+=getPaddingBottom();
            setMeasuredDimension(getDefaultSize(getSuggestedMinimumWidth(),widthMeasureSpec),realHeight);
        }
        else super.onMeasure(widthMeasureSpec,heightMeasureSpec);
    }

    /**
     * 设置左端最小值.如果设置的值超过最大值,最大值将扩展到此值+1
     * @param min 最小值
     */
    public void setMin(int min) {
        mMin = min;
        if (mMin > mMax)
            mMax = mMin + 1;
        invalidate();
    }

    /**
     * 设置右端最大值.如果设置的值小于最小值,最小值缩小到此值-1
     * @param max 最小值
     */
    public void setMax(int max) {
        mMax = max;
        if (mMax < mMin)
            mMin = mMax - 1;
        invalidate();
    }

    public void setLeftValue(int value) {
        mCurrLeftValue = value;
        if (mCurrLeftValue < mMin)
            mCurrLeftValue = mMin;
        invalidate();
    }

    public void setRightValue(int value) {
        mCurrRightValue = value;
        if (mCurrRightValue > mMax)
            mCurrRightValue = mMax;
        invalidate();
    }

    public void setLeftCircleColor(@ColorInt int color){
        mLeftCirclePaint.setColor(color);
        invalidate();
    }

    public void setRightCircleColor(@ColorInt int color){
        mRightCirclePaint.setColor(color);
        invalidate();
    }

    public void setBackgroundColor(@ColorInt int color){
        mBackgroundLinePaint.setColor(color);
        invalidate();
    }

    public void setProgressColor(@ColorInt int color){
        mProgressPaint.setColor(color);
        invalidate();
    }

    public void setOnProgressChangedListener(OnProgressChanged listener) {
        mListener = listener;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mCurrLeftValue != -1)
            mLeftCirclePosition = mCurrLeftValue * getWidth() / (mMax - mMin);
        if (mCurrRightValue != -1)
            mRightCirclePosition = mCurrRightValue * getWidth() / (mMax - mMin);
        float leftCirclePosition = mLeftCirclePosition;
        float rightCirclePosition = mRightCirclePosition;

        if (leftCirclePosition < RADIUS) leftCirclePosition = RADIUS;
        else if (leftCirclePosition > getWidth() - RADIUS) leftCirclePosition = getWidth() - RADIUS;
        if (rightCirclePosition < RADIUS) rightCirclePosition = RADIUS;
        else if (rightCirclePosition > getWidth() - RADIUS)
            rightCirclePosition = getWidth() - RADIUS;

        canvas.drawRect(0, getHeight() / 2f - 2, getWidth(), getHeight() / 2f + 2, mBackgroundLinePaint);
        canvas.drawRect(leftCirclePosition, getHeight() / 2f - 2, rightCirclePosition, getHeight() / 2f + 2, mProgressPaint);
        canvas.drawCircle(leftCirclePosition, getHeight() / 2f, RADIUS, mLeftCirclePaint);
        canvas.drawCircle(rightCirclePosition, getHeight() / 2f, RADIUS, mRightCirclePaint);

        float rightValue = mRightCirclePosition / getWidth();
        if (rightValue > 0.99) rightValue = 1;

        float leftValue = mLeftCirclePosition / getWidth();
        if (leftValue < 0) leftValue = 0;
        if (mListener != null)
            mListener.progressChanged(mCurrLeftValue != -1 || mCurrRightValue != -1, mMin + (int) (leftValue * (mMax - mMin)), mMin + (int) (rightValue * (mMax - mMin)));
        mCurrLeftValue = -1;
        mCurrRightValue = -1;
    }

    public interface OnProgressChanged {
        void progressChanged(boolean fromUser, int leftValue, int rightValue);
    }
}