package com.fastlib.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;

import com.fastlib.R;
import com.fastlib.annotation.ContentView;
import com.fastlib.app.module.SupportBack;
import com.fastlib.utils.ScreenUtils;
import com.fastlib.utils.bind_view.ViewInject;

/**
 * Created by sgfb on 16/9/20.
 * 以Fragment实现底部dialog，带动画
 */
public abstract class BottomDialog extends Fragment implements SupportBack {
    public static final String ARG_INT_COLOR ="colorId";

    private ObjectAnimator mStartAnimator,mBgAnimation;
    private View mView;
    private View mBg;

    /**
     * 绑定视图
     */
    protected abstract void bindView();

    @Override
    public boolean onBackPressed() {
        return false;
    }

    public void show(FragmentManager fm){
        fm.beginTransaction()
                .replace(android.R.id.content,this)
                .commit();
    }

    /**
     * 获取对应的布局id
     * @return 布局id
     * @throws IllegalArgumentException 如果未设置ContentView弹出异常
     */
    private int getLayoutId()throws IllegalArgumentException{
        ContentView cv=getClass().getAnnotation(ContentView.class);
        if(cv!=null) return cv.value();
        else throw new IllegalArgumentException("未设置BottomDialog的ContentView");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        ViewGroup parent= createMainView();
        FrameLayout.LayoutParams lp=new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,FrameLayout.LayoutParams.WRAP_CONTENT);
        int bgColor=-1;
        mBg=parent.getChildAt(0);
        mView=inflater.inflate(getLayoutId(),null);

        if(getArguments()!=null)
            bgColor=getArguments().getInt(ARG_INT_COLOR,-1);
        if(bgColor!=-1)
            mBg.setBackgroundColor(bgColor);
        mView.setLayoutParams(lp);
        mView.setTranslationY(ScreenUtils.getScreenHeight());

        parent.addView(mView);
        ViewInject.inject(this,parent);
        bindView();
        mBg.setOnClickListener(v -> dismiss());
        mView.setOnClickListener(v -> {
            //防止点击穿透
        });
        return parent;
    }

    private ViewGroup createMainView(){
        FrameLayout mainView=new FrameLayout(getContext());
        View bg=new View(getContext());

        bg.setBackgroundColor(Color.parseColor("#99000000"));
        bg.setAlpha(0);
        mainView.addView(bg);
        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mView.setFocusableInTouchMode(true);
        mView.requestFocus();
        mView.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode==KeyEvent.KEYCODE_BACK&&event.getAction()==KeyEvent.ACTION_UP){
                dismiss();
                return true;
            }
            return false;
        });
        mView.post(() -> {
            View parent= (View) mView.getParent();
            mStartAnimator=ObjectAnimator.ofFloat(mView,"translationY",mView.getTranslationY(),parent.getBottom()-mView.getHeight()).setDuration(220);
            mBgAnimation=ObjectAnimator.ofFloat(mBg,"alpha",0,1).setDuration(220);
            mStartAnimator.start();
            mBgAnimation.start();
        });
    }

    public void dismiss(){
        if(mStartAnimator!=null){
            mStartAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    if(getActivity()!=null)
                        getFragmentManager().beginTransaction().remove(BottomDialog.this).commit();
                }
            });
            mBgAnimation.reverse();
            mStartAnimator.reverse();
        }
        else getFragmentManager().beginTransaction().remove(this).commit();
    }
}