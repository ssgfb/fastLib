package com.fastlib.bean.event;

/**
 * Created by sgfb on 16/10/10.
 * 上传文件时发送的广播
 */
public class EventUploading{
    private long mSendByte; //已发送字节
    private String mPath;

    public EventUploading(long sendByte,String path){
        this.mPath=path;
        this.mSendByte =sendByte;
    }

    public long getSendByte() {
        return mSendByte;
    }

    public String getPath() {
        return mPath;
    }
}
