package com.fastlib.aspect.component.opaque_action;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.fastlib.aspect.AspectManager;
import com.fastlib.aspect.component.SimpleAspectAction;
import com.fastlib.aspect.component.impl.ActivityResultCallback;
import com.fastlib.aspect.component.inject.SycStartActivityForResult;

import java.lang.reflect.Method;

/**
 * Created by sgfb on 2020\03\13.
 */
public class SycStartActivityForResultAction2 extends SimpleAspectAction<SycStartActivityForResult> implements ActivityResultCallback {
    private SycStartActivityForResult mAnno;

    @Override
    protected void handle(SycStartActivityForResult anno, Method method, Object[] args) {
        mAnno=anno;
        AspectManager.getInstance().addBroadcastLockAction(ActivityResultCallback.class,this);

        Activity activity=getEnv(Activity.class);
        activity.startActivityForResult(new Intent(activity,anno.value()),getLockId());
        setPassed(true);
        lock();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==getLockId()&&resultCode==Activity.RESULT_OK&&data!=null){
            Bundle bundle=data.getExtras();
            setResult(bundle!=null?bundle.get(mAnno.resultKey()):null);
        }
        unlock();   //统一放AspectBroadcast里？
    }
}
