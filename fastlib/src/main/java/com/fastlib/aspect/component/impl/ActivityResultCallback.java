package com.fastlib.aspect.component.impl;

import android.content.Intent;

/**
 * Created by sgfb on 2020\03\13.
 * 接收Activity.onActivityResult事件
 */
public interface ActivityResultCallback{

    void onActivityResult(int requestCode, int resultCode, Intent data);
}
