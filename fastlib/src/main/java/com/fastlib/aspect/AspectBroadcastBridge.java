package com.fastlib.aspect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sgfb on 2020\03\12.
 * 切面广播桥接.通常由事件方发送一些广播给切面任务执行器,
 * 例如Activity.onActivityResult事件发送给{@link com.fastlib.aspect.component.opaque_action.SycStartActivityForResultAction}
 */
public class AspectBroadcastBridge{
    private Map<Class,List<Object>> mTypeToLockAction =new HashMap<>();    //广播事件-->锁定等待事件的切面执行器
    private Map<Class,Object> mCacheInstance=new HashMap<>();              //广播事件-->广播事件执行实例缓存

    @SuppressWarnings("unchecked")
    public <T> T getEventReceiver(Class<T> cla){
        T instance= (T) mCacheInstance.get(cla);
        if(instance==null){
            instance= (T) Proxy.newProxyInstance(cla.getClassLoader(), new Class[]{cla}, new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable{
                    method.setAccessible(true);

                    List<T> lockActions= (List<T>) mTypeToLockAction.get(cla);
                    if(lockActions!=null){
                        for(T t:lockActions){
                            method.invoke(t,args);
                        }
                        lockActions.clear();
                    }
                    return null;
                }
            });
            mCacheInstance.put(cla,instance);
        }
        return instance;
    }

    public void addLockAction(Class eventType,Object lockAction){
        if(!eventType.isInterface()) throw new IllegalArgumentException("参数eventType必须是接口");

        List<Object> lockActions= mTypeToLockAction.get(eventType);
        if(lockActions==null){
            lockActions=new ArrayList<>();
            mTypeToLockAction.put(eventType,lockActions);
        }
        lockActions.add(lockAction);
    }
}
