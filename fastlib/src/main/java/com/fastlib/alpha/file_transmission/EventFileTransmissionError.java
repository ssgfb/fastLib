package com.fastlib.alpha.file_transmission;

import com.fastlib.net.Request;

/**
 * Created by sgfb on 2020\04\05.
 * 文件传输服务异常消息
 */
public class EventFileTransmissionError{
    private String mKey;
    private Request mRequest;
    private Exception mException;

    public EventFileTransmissionError(String mKey, Request mRequest, Exception mException) {
        this.mKey = mKey;
        this.mRequest = mRequest;
        this.mException = mException;
    }

    public String getKey() {
        return mKey;
    }

    public Request getRequest() {
        return mRequest;
    }

    public Exception getException() {
        return mException;
    }
}
