package com.fastlib.alpha.file_transmission;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.fastlib.app.EventObserver;
import com.fastlib.db.And;
import com.fastlib.db.Condition;
import com.fastlib.db.FastDatabase;
import com.fastlib.net.Request;
import com.fastlib.net.RequestStatusReader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import androidx.annotation.Nullable;

/**
 * Created by sgfb on 2020\03\13.
 * 文件传输服务.描述文件传输的启动、暂停和停止以及这些事件的历史记录保存
 *
 * 接收和发送的指令
 * 1.启动（包括未开始的和已暂停的）文件传输
 * 2.暂停文件传输
 * 3.删除某文件上传下载请求
 */
public abstract class FileTransmissionService extends Service{
    public static final String TAG= FileTransmissionService.class.getSimpleName();

    /**
     * 指令.启动上传下载或暂停或删除
     */
    public static final String ARG_STR_COMMAND="command";

    public static final String COMMAND_START="commandStart";                //激活任务命令(如果请求在队列中是继续任务,不在队列中则生成并开始)
    public static final String COMMAND_PAUSE="commandPause";                //暂停任务命令
    public static final String COMMAND_DELETE="commandDelete";              //删除任务命令
    public static final String COMMAND_START_ALL="commandStartAll";         //激活所有任务
    public static final String COMMAND_PAUSE_ALL="commandPauseAll";         //暂停所有任务

    private Map<String,Request> mRequestMap =new HashMap<>();
    private ThreadPoolExecutor mThreadPool;

    /**
     * 仅在首次启动时配置线程池
     */
    protected ThreadPoolExecutor configThreadPool(){
        return (ThreadPoolExecutor) Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()+2);
    }

    /**
     * 生成网络请求对应的唯一key.同key将视为同样的请求
     * @return 对应网络请求的唯一key
     */
    protected abstract String genRequestKey();

    /**
     * 生成网络请求
     */
    protected abstract Request genRequestByKey(String key);

    /**
     * 清理移除的请求任务
     * @param request 网络请求
     */
    protected abstract void clearRequest(Request request);

    @Override
    public void onCreate() {
        super.onCreate();
        mThreadPool=configThreadPool();
        mThreadPool.execute(() -> {
            List<FileTransmissionHistory> historyList=FastDatabase.getDefaultInstance(FileTransmissionService.this).get(FileTransmissionHistory.class);
            if(historyList!=null){
                for(FileTransmissionHistory history:historyList){
                    mRequestMap.put(history.key, genRequestByKey(history.key));
                }
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        String command=intent.getStringExtra(ARG_STR_COMMAND);

        switch (command){
            case COMMAND_START:
                startTransmission();
                break;
            case COMMAND_PAUSE:
                pauseTransmission();
                break;
            case COMMAND_DELETE:
                deleteTransmission();
                break;
            default:
                Log.w(TAG,"文件传输命令 "+command+" 无法识别");
                break;
        }
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 如果任务在队列中且未运行则继续任务，如果没在队列中新起一个请求且开始
     */
    private void startTransmission(){
        final String key=genRequestKey();
        Request request= mRequestMap.get(key);

        if(request==null) {
            request = genRequestByKey(key);
            mRequestMap.put(key,request);
        }
        String status=request.getStatus();
        if(TextUtils.equals(status,RequestStatusReader.STATUS_READY)||
                TextUtils.equals(status,RequestStatusReader.STATUS_CANCELED)||
                TextUtils.equals(status,RequestStatusReader.STATUS_COMPLETED)){ //判断complete主要是为了在异常时也能继续任务
            Request finalRequest = request;
            mThreadPool.execute(() -> {
                try {
                    FileTransmissionHistory record=new FileTransmissionHistory(key);
                    FastDatabase.getDefaultInstance(FileTransmissionService.this).saveOrUpdate(record);
                    finalRequest.startSync();
                    mRequestMap.remove(key);
                    FastDatabase.getDefaultInstance(FileTransmissionService.this).delete(record);
                } catch (Exception e) {
                    EventObserver.getInstance().sendEvent(FileTransmissionService.this,new EventFileTransmissionError(key,finalRequest,e));
                }
            });
        }
        else
            Log.w(TAG,"任务运行中或已完结");
    }

    /**
     * 暂停一个任务
     */
    private void pauseTransmission(){
        Request request= mRequestMap.get(genRequestKey());

        if(request!=null){
            request.cancel();
        }
    }

    /**
     * 删除一个任务.将会清理任务相关文件,具体操作依赖子类实现{@link #clearRequest(Request)}
     */
    private void deleteTransmission(){
        String key=genRequestKey();
        Request request= mRequestMap.remove(key);

        if(request!=null){
            request.cancel();
            FileTransmissionHistory record=FastDatabase.getDefaultInstance(this).addFilter(And.condition(Condition.equal("key",key))).getFirst(FileTransmissionHistory.class);
            if(record!=null)
                FastDatabase.getDefaultInstance(this).delete(record);
            clearRequest(request);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
