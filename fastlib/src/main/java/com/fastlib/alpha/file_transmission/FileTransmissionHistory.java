package com.fastlib.alpha.file_transmission;

import com.fastlib.annotation.Database;

/**
 * Created by sgfb on 2020\04\05.
 * 文件传输记录
 */
public class FileTransmissionHistory{
    @Database(keyPrimary = true)
    public String key;

    public FileTransmissionHistory(){}

    public FileTransmissionHistory(String key) {
        this.key = key;
    }
}
