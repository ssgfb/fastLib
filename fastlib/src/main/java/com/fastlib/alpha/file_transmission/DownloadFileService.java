package com.fastlib.alpha.file_transmission;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.fastlib.app.EventObserver;
import com.fastlib.bean.event.EventDownloading;
import com.fastlib.net.Request;
import com.fastlib.net.download.DownloadMonitor;
import com.fastlib.net.download.SingleDownloadController;

import java.io.File;

import androidx.annotation.NonNull;

/**
 * Created by sgfb on 2020\04\04.
 * 文件下载队列后台服务
 * TODO 过期判断
 */
public class DownloadFileService extends FileTransmissionService {
    public static final String ARG_STR_URL ="url";
    public static final String ARG_STR_FILE_PATH ="filePath";

    private String mUrl;
    private String mFilePath;

    public static void addDownloadRequest(Context context, String url, String filePath){
        Intent intent=new Intent(context,DownloadFileService.class);
        intent.putExtra(ARG_STR_COMMAND,COMMAND_START);
        intent.putExtra(ARG_STR_URL,url);
        intent.putExtra(ARG_STR_FILE_PATH,filePath);
        context.startService(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mUrl=intent.getStringExtra(ARG_STR_URL);
        mFilePath=intent.getStringExtra(ARG_STR_FILE_PATH);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected String genRequestKey(){
        return mUrl+"_"+mFilePath;
    }

    @Override
    protected Request genRequestByKey(String key){
        String[] params=key.split("_");
        Request request=new Request(params[0]);
        SingleDownloadController downloadController=new SingleDownloadController(new File(params[1]));

        downloadController.setDownloadMonitor(new DownloadMonitor() {
            @Override
            protected void onDownloading(long downloadedOneInterval) {
                EventObserver.getInstance().sendEvent(DownloadFileService.this,new EventDownloading(mExpectDownloadSize,
                        (int)downloadedOneInterval,
                        params[1],
                        request));
            }

            @Override
            protected long downloadedSize() {
                return mFile.length();
            }
        });
        request.setDownloadable(downloadController);
        adjustRequest(request);
        return request;
    }

    /**
     * 作为后期绑定增加Request灵活性
     */
    protected void adjustRequest(@NonNull Request request){}

    @Override
    protected void clearRequest(Request request){
        if(!new File(mFilePath).delete())
            Log.w(TAG,"文件"+mFilePath+"未清理成功");
        if(!request.getDownloadable().getOutputFile().delete())
            Log.w(TAG,"文件"+request.getDownloadable().getOutputFile()+"未清理成功");
    }
}
