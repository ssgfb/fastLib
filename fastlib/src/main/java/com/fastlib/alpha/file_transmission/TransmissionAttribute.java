package com.fastlib.alpha.file_transmission;

/**
 * Created by sgfb on 2020\04\04.
 * 文件传输属性
 */
public class TransmissionAttribute{
    public static final int TRANSMISSION_TYPE_DOWNLOAD=1;   //传输类型下载
    public static final int TRANSMISSION_TYPE_UPLOAD=2;     //传输类型上传

    private int transmissionType;
    private String key;

    public TransmissionAttribute(int transmissionType, String key) {
        this.transmissionType = transmissionType;
        this.key = key;
    }

    public int getTransmissionType() {
        return transmissionType;
    }

    public boolean isDownloadType(){
        return transmissionType==TRANSMISSION_TYPE_DOWNLOAD;
    }

    public boolean isUploadType(){
        return transmissionType==TRANSMISSION_TYPE_UPLOAD;
    }

    public String getKey() {
        return key;
    }
}
