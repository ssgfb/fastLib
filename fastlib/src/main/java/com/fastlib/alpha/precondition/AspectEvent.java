package com.fastlib.alpha.precondition;

import android.util.SparseArray;
import androidx.annotation.NonNull;
import com.fastlib.aspect.component.AspectRuntimeArg;
import com.fastlib.aspect.exception.EnvMissingException;
import com.fastlib.aspect.exception.LockNotFoundException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 切面事件基类
 */
public abstract class AspectEvent<T extends Annotation>{
    private static final List<CrossLock> sIdleLock=new ArrayList<>();
    private static final SparseArray<CrossLock> sUsedLock =new SparseArray<>();

    private boolean conditionResulted;
    private boolean conditionPass;
    private CrossLock currentLock;

    protected abstract void doAction(T condition) throws Throwable;

    protected synchronized void setCondition(boolean conditionPass){
        if(conditionResulted)
            return;
        this.conditionPass=conditionPass;
        currentLock.unlock();
        conditionResulted=true;
    }

    private CrossLock obtainLock(){
        CrossLock resultLock;
        if(!sIdleLock.isEmpty()){
            resultLock=sIdleLock.remove(sIdleLock.size()-1);
            resultLock.reset();
        }
        else resultLock=new CrossLock();

        sUsedLock.put(resultLock.id,resultLock);
        return resultLock;
    }

    /**
     * 所有事件执行前都会上锁,在结尾处解锁
     */
    private static class CrossLock{
        private static AtomicInteger autoIncrease=new AtomicInteger();

        private int id;
        private boolean unlocked;

        public CrossLock() {
            id = autoIncrease.addAndGet(1);
        }

        public int getId(){
            return id;
        }

        public void lock(){
            if(unlocked)
                return;
            synchronized (CrossLock.this){
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void unlock(){
            unlocked=true;
            synchronized (CrossLock.this){
                backLock(id);
                notify();
            }
        }

        private void reset(){
            id = autoIncrease.addAndGet(1);
            unlocked=false;
        }

        private void backLock(int id){
            CrossLock crossLock=sUsedLock.get(id);
            if(crossLock!=null){
                sUsedLock.remove(id);
                sIdleLock.add(crossLock);
            }
            else throw new LockNotFoundException();
        }
    }

    public boolean start(T condition,List envs){
        injectArgs(envs);
        currentLock=obtainLock();
        try {
            doAction(condition);
        } catch (Throwable throwable) {
            //TODO 调起异常回调逻辑链
        }
        currentLock.lock();
        return conditionPass;
    }

    /**
     * 注入运行时参数
     */
    private void injectArgs(List evns){
        Field[] fields=getClass().getDeclaredFields();

        for(Field field:fields){
            AspectRuntimeArg runtimeArg=field.getAnnotation(AspectRuntimeArg.class);
            if(runtimeArg!=null){
                try {
                    field.setAccessible(true);
                    field.set(this,getEnv(evns,runtimeArg.value()));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 获取环境参数
     */
    @SuppressWarnings("unchecked")
    private @NonNull <E> E getEnv(List envs,Class<E> cla){
        if(envs !=null){
            for(Object env: envs){
                if(cla.isInstance(env)){
                    return (E) env;
                }
            }
        }
        throw new EnvMissingException("事件"+getClass().getSimpleName()+"环境参数缺失"+cla.getSimpleName());
    }

    public int getId(){
        return currentLock.id;
    }
}
