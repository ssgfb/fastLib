package com.fastlib.alpha.precondition;

import android.content.pm.PackageManager;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import com.fastlib.aspect.component.AspectRuntimeArg;
import com.fastlib.aspect.component.PermissionResultReceiverGroup;
import com.fastlib.aspect.event_callback.ThirdParamReceiver;

/**
 * 运行时权限条件校验
 */
public class PermissionCondition extends AspectEvent<Permission>{
    @AspectRuntimeArg(AppCompatActivity.class)
    AppCompatActivity activity;
    @AspectRuntimeArg(PermissionResultReceiverGroup.class)
    PermissionResultReceiverGroup permissionResultReceiverGroup;

    @Override
    protected void doAction(Permission condition) throws Throwable {
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M)
            setCondition(true);
        else{
            permissionResultReceiverGroup.addEventCallback((param1, param2, param3) -> {
                for(int grant:param3){
                    if(grant!= PackageManager.PERMISSION_GRANTED){
                        setCondition(false);
                        return;
                    }
                }
                setCondition(true);
            });
            activity.requestPermissions(condition.value(),1);
        }
    }
}
