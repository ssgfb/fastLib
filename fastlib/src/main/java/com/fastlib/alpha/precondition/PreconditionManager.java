package com.fastlib.alpha.precondition;

import android.util.Log;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

/**
 * 预条件管理器.管理配置和一些全局仓库
 */
public class PreconditionManager{
    private static final String TAG=PreconditionManager.class.getName();
    private static PreconditionManager sInstance;
    private Map<Class<? extends Annotation>,AspectEvent> eventMap;
    private Map<String,Object> conditionVerifyMap;
    private PreconditionExceptionListener globalExceptionListener;

    private PreconditionManager(){
        eventMap=new HashMap<>();
        conditionVerifyMap=new HashMap<>();
    }

    public static PreconditionManager getInstance(){
        if(sInstance==null){
            synchronized (PreconditionManager.class){
                if(sInstance==null)
                    sInstance =new PreconditionManager();
            }
        }
        return sInstance;
    }

    public void putAspectEvent(Class<? extends Annotation> condition,AspectEvent event){
        Log.d(TAG,"增加事件 "+condition+"------>"+event.getClass().getName());
        eventMap.put(condition,event);
    }

    public AspectEvent getAspectEvent(Class<? extends Annotation> condition){
        return eventMap.get(condition);
    }

    public void putConditionVerify(Object conditionVerify){
        Class[] interfaces=conditionVerify.getClass().getInterfaces();
        for(Class inter:interfaces)
            conditionVerifyMap.put(inter.getCanonicalName(),conditionVerify);
    }

    @SuppressWarnings("unchecked")
    public <T> T getConditionVerify(String name){
        return (T) conditionVerifyMap.get(name);
    }

    public PreconditionExceptionListener getGlobalExceptionListener() {
        return globalExceptionListener;
    }

    public void setGlobalExceptionListener(PreconditionExceptionListener globalExceptionListener) {
        this.globalExceptionListener = globalExceptionListener;
    }
}
