package com.fastlib.alpha.precondition;

import android.util.SparseArray;
import android.util.SparseBooleanArray;
import androidx.core.util.Pair;
import com.fastlib.app.ThreadPoolManager;
import leo.android.cglib.proxy.MethodInterceptor;
import leo.android.cglib.proxy.MethodProxy;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

/**
 * 预条件方法拦截器
 * 1.事件运行在工作线程
 * 2.对执行方法进行预置条件检查时是没有返回的
 * 3.不允许同时多次运行
 */
public class PreconditionMethodInterceptor implements MethodInterceptor {
    private final SparseBooleanArray runningMap=new SparseBooleanArray();

    @Override
    public Object intercept(Object o, Object[] objects, MethodProxy methodProxy) throws Exception {
        if(runningMap.get(methodProxy.getOriginalMethod().hashCode()))
            return false;
        runningMap.put(methodProxy.getOriginalMethod().hashCode(),true);
        Annotation[] annotations=methodProxy.getOriginalMethod().getAnnotations();
        List<Pair<Annotation,AspectEvent>> aspectEvents=new ArrayList<>();
        for(Annotation annotation:annotations){
            AspectEvent event=PreconditionManager.getInstance().getAspectEvent(annotation.annotationType());
            if(event!=null)
                aspectEvents.add(Pair.create(annotation,event));
        }
        if(aspectEvents.isEmpty()) {
            runningMap.put(methodProxy.getOriginalMethod().hashCode(),false);
            return methodProxy.invokeSuper(o, objects);
        }
        else{
            ThreadPoolManager.sSlowPool.execute(() -> {
                boolean conditionPass=true;
                for(Pair<Annotation,AspectEvent> pair:aspectEvents){
                    conditionPass=pair.second.start(pair.first,((EnvGetter)objects[0]).getEnv());
                    if(!conditionPass)
                        break;
                }
                if(conditionPass)
                    methodProxy.invokeSuper(o,objects);
                runningMap.put(methodProxy.getOriginalMethod().hashCode(),false);
            });
        }
        //TODO 生命周期管理
        return null;
    }
}
