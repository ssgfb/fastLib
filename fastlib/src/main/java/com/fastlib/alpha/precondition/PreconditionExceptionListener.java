package com.fastlib.alpha.precondition;

import java.lang.annotation.Annotation;

/**
 * 当预条件异常或者未通过时回调
 */
public interface PreconditionExceptionListener{

    /**
     * 异常或未通过时回调此方法
     * @param precondition  指明预条件类型
     * @param throwable     如果为空说明是未通过,非空则是异常
     */
    void onException(Class<? extends Annotation> precondition,Throwable throwable);
}
