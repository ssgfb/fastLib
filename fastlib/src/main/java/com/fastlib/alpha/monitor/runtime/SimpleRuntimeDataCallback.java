package com.fastlib.alpha.monitor.runtime;

/**
 * Created by sgfb on 2020\04\15.
 * 数据可变更的运行时数据回调
 */
public abstract class SimpleRuntimeDataCallback implements RuntimeDataCallback{

    @Override
    public boolean canChange() {
        return true;
    }
}
