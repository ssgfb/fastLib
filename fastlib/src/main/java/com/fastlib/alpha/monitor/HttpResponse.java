package com.fastlib.alpha.monitor;

import com.fastlib.db.SaveUtil;
import com.fastlib.net.core.HeaderDefinition;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.security.cert.CRL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by sgfb on 2020\04\17.
 * Http返回包裹
 */
public class HttpResponse{
    private static final String CRLF = "\r\n";

    private int resultCode;
    private Map<String, List<String>> header;
    private List<InputStream> body;

    public HttpResponse() {
        resultCode= HttpURLConnection.HTTP_OK;
        header=new HashMap<>();
        body=new ArrayList<>();
    }

    /**
     * 开始服务端返回
     * @param out 服务端输出流
     */
    public void serverResponse(OutputStream out)throws IOException {
        Map<String,List<String>> headerMap=genStandardHeader();

        StringBuilder sb=new StringBuilder();
        sb.append("HTTP/1.1 ").append(resultCode).append(CRLF);

        for(Map.Entry<String,List<String>> entry:headerMap.entrySet()){
            List<String> value=entry.getValue();
            sb.append(entry.getKey()).append(": ").append(formatWithSign(value,";")).append(CRLF);
        }
        sb.append(CRLF);

        out.write(sb.toString().getBytes());
        for(InputStream in:body){
            byte[] buffer=new byte[4096];
            int len;
            while((len=in.read(buffer))!=-1)
                out.write(buffer,0,len);
            in.close();
        }
        out.write(CRLF.getBytes());
    }

    /**
     * 服务端返回头，包含了必要的头部
     */
    private Map<String,List<String>> genStandardHeader() throws IOException {
        Map<String,List<String>> map=new HashMap<>();
        map.put("Content-Type", Arrays.asList("text/html","Charset=utf-8"));
        map.put("Content-Length",Arrays.asList(Long.toString(bodyLength())));
        map.put("Access-Control-Allow-Origin",Arrays.asList("*"));
        map.putAll(header);
        return map;
    }

    private long bodyLength() throws IOException {
        long count=0;
        for(InputStream in:body){
            count+=in.available();
        }
        return count;
    }

    /**
     * 将字符串列表转换成单字符串，且以指定符号间隔
     */
    private String formatWithSign(@Nullable List<String> list,@NonNull String sign){
        if(list==null||list.isEmpty()) return "";

        StringBuilder sb=new StringBuilder();
        for(String s:list)
            sb.append(s).append(sign);
        if(sb.length()>0)
            sb.deleteCharAt(sb.length()-1);
        return sb.toString();
    }

    public void addHeader(String key,String value){
        List<String> values=header.get(key);
        if(values==null){
            values=new ArrayList<>();
            header.put(key,values);
        }
        values.add(value);
    }

    public void addBody(InputStream inputStream){
        body.add(inputStream);
    }

    public void addBody(@NonNull String value){
        body.add(new ByteArrayInputStream(value.getBytes()));
    }

    public void setBody(@NonNull InputStream in){
        body.clear();
        addBody(in);
    }

    public void setBody(@NonNull String value){
        body.clear();
        addBody(value);
    }
}
