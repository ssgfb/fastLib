package com.fastlib.alpha.monitor;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.fastlib.app.ThreadPoolManager;

import androidx.annotation.Nullable;

/**
 * Created by sgfb on 2020\04\13.
 * 直接运行在工作线程中的命令服务，不使用绑定服务
 */
public abstract class AsyncCommandService extends Service {

    protected abstract void startCommand(Intent intent,int flags,int startId);

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ThreadPoolManager.sSlowPool.execute(() -> startCommand(intent,flags,startId));
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
