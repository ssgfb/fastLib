package com.fastlib.alpha.monitor.file;

/**
 * Created by sgfb on 2020\04\15.
 */
public class FileInfo{
    public boolean isFolder;
    public long size;
    public long lastModifier;
    public String name;

    public FileInfo(boolean isFolder,long size, long lastModified, String name) {
        this.isFolder=isFolder;
        this.size = size;
        this.lastModifier = lastModified;
        this.name = name;
    }
}
