package com.fastlib.alpha.monitor;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.google.gson.Gson;

import java.io.IOException;

public abstract class JsonServerHandle extends StringServerHandle{

    protected abstract Object handleResult(HttpClientRequest request)throws IOException;

    @Override
    protected String handle(HttpClientRequest request) throws IOException{
        Gson gson=new Gson();
        return gson.toJson(handleResult(request));
    }
}
