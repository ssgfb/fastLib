package com.fastlib.alpha.monitor.file;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.alpha.monitor.HttpResponse;
import com.fastlib.alpha.monitor.ServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.fastlib.db.SaveUtil;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import androidx.core.util.Pair;

/**
 * Created by sgfb on 2020\04\18.
 * 接收来自客户端的数据并保存为指定路径文件(客户端上传)
 */
@Path("/file/upload")
public class FileReceiveProvider implements ServerHandle {

    @Override
    public HttpResponse interaction(HttpClientRequest request) throws IOException{
        String savePath=request.getParamFirst("path");
        String errorMsg= checkFolder(savePath);

        HttpResponse response=new HttpResponse();
        if(errorMsg==null){
            File folder=new File(savePath);
            for(Map.Entry<String, Pair<String,File>> entry:request.getFiles().entrySet()){
                File file=new File(folder,entry.getValue().first);
                file.createNewFile();
                SaveUtil.copyFileTo(entry.getValue().second,file);
            }
            response.setBody("上传成功");
        }
        else
            response.setBody(errorMsg);
        return response;
    }

    private String checkFolder(String path){
        if(path==null)
            return "path不能为空";

        File folder=new File(path);

        if(folder.exists()){
            if(folder.isFile())
                return "该路径为文件，请指定文件夹路径";
        }
        else{
            if(!folder.mkdirs()){
                return "创建文件夹失败";
            }
        }
        return null;
    }
}
