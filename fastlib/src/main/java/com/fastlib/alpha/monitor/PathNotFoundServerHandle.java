package com.fastlib.alpha.monitor;

import com.fastlib.alpha.monitor.request.HttpClientRequest;

import java.io.IOException;

/**
 * Created by sgfb on 2020\04\13.
 * 未找到逻辑类型提供的错误提示
 */
public class PathNotFoundServerHandle extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) throws IOException {
        return "未找到指定路径";
    }
}
