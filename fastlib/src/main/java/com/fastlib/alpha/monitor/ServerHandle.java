package com.fastlib.alpha.monitor;

import com.fastlib.alpha.monitor.request.HttpClientRequest;

import java.io.IOException;

/**
 * Created by sgfb on 2020\04\13.
 * 与远程交互逻辑实现
 * 接收一远程请求参数并且返回数据
 */
public interface ServerHandle {

    HttpResponse interaction(HttpClientRequest request)throws IOException;
}
