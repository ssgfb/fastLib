package com.fastlib.alpha.monitor;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.db.SaveUtil;
import com.fastlib.utils.ContextHolder;

import java.io.IOException;

/**
 * Created by sgfb on 2020\04\20.
 */
@Path("/")
public class IndexServerHandle extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) throws IOException{
        return new String(SaveUtil.loadInputStream(ContextHolder.getContext().getAssets().open("Index.html"),true));
    }
}
