package com.fastlib.alpha.monitor.runtime;

import android.text.TextUtils;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sgfb on 2020\04\15.
 * 运行时数据交互提供器
 */
@Path("/runtime/show")
public class RuntimeDataServerHandle extends StringServerHandle {
    private final static Map<String,RuntimeDataCallback> sCallbackMap=new HashMap<>();

    public static void registerRuntimeDataCallback(String name,RuntimeDataCallback callback){
        sCallbackMap.put(name,callback);
    }

    public static void unregisterRuntimeDataCallback(String name){
        sCallbackMap.remove(name);
    }

    @Override
    protected String handle(HttpClientRequest request) throws IOException {
        String name=request.getParamFirst("name");
        String newValue=request.getParamFirst("newValue");

        if(TextUtils.isEmpty(name))
            return "name字段不能为空";
        RuntimeDataCallback callback=sCallbackMap.get(name);
        if(callback!=null) {
            if(!TextUtils.isEmpty(newValue)){
                if(callback.canChange())
                    callback.changeTo(newValue);
                else
                    return "此运行时数据不可变更";
            }
            return callback.showData();
        }
        return "此名称未注册运行时数据显示:"+name;
    }
}
