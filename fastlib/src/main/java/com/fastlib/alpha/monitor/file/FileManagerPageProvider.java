package com.fastlib.alpha.monitor.file;

import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.db.SaveUtil;
import com.fastlib.utils.ContextHolder;

import java.io.IOException;

@Path("/file/index")
public class FileManagerPageProvider extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) throws IOException{
        return new String(SaveUtil.loadInputStream(ContextHolder.getContext().getAssets().open("FileManager.html"),true));
    }
}
