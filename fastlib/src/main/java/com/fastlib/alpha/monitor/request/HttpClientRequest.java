package com.fastlib.alpha.monitor.request;

import android.content.Context;
import android.text.TextUtils;

import com.fastlib.alpha.monitor.HttpClientRequestParserException;
import com.fastlib.net.core.HeaderDefinition;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.core.util.Pair;

/**
 * Created by sgfb on 2020\04\13.
 * 类Http协议客户端请求解析结果
 */
public class HttpClientRequest {
    private String path;
    private String host;
    private String protocol;
    private Map<String,List<String>> header;
    private Map<String,List<String>> params;
    private Map<String,Pair<String,File>> files;

    private HttpClientRequest(){
        header=new HashMap<>();
        params=new HashMap<>();
        files=new HashMap<>();
    }

    public static HttpClientRequest parserClientRequest(Context context,InputStream in)throws IOException{
        HttpClientRequest request=new HttpClientRequest();
        InputStream bufferIn=new BufferedInputStream(in);

        String firstLine=readLine(bufferIn);
        if(TextUtils.isEmpty(firstLine))
            throw new HttpClientRequestParserException("首行为空");
        String[] firstSplit=firstLine.split(" ");
        if(firstSplit.length<3)
            throw new HttpClientRequestParserException("首行格式异常:"+firstLine);
        request.path=request.splitPath(firstSplit[1]);
        request.inflaterParams(firstSplit[1]);
        request.protocol=firstSplit[2];

        String line;
        while (!TextUtils.isEmpty(line=readLine(bufferIn))){
            String[] headerSplit=line.split(":");
            if(headerSplit.length<2){
                //抛出异常更好点？
                continue;
            }
            String key=headerSplit[0];
            String valuesStr=headerSplit[1];
            String[] values=valuesStr.split(",");
            List<String> valueList=request.header.get(key);

            if(valueList==null){
                valueList=new ArrayList<>();
                request.header.put(key.toLowerCase(),valueList);
            }
            for (String value : values) {
                if (value != null)
                    valueList.add(value.trim());
            }
        }
        request.host=request.getFirstHeader(HeaderDefinition.KEY_HOST);
        request.parseBody(context,bufferIn);
        return request;
    }

    private static String readLine(InputStream in) throws IOException {
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        int c;

        while((c=in.read())!=-1){
            if(c=='\n')
                break;
            baos.write(c);
        }

        String s=baos.toString();
        if("\r".equals(s)||(s.length()>0&&s.charAt(s.length()-1)=='\r'))
            return s.substring(0,s.length()-1);
        return s;
    }

    private String getFirstHeader(String key){
        String value=null;
        List<String> values=header.get(key!=null?key.toLowerCase():null);
        if(values!=null&&!values.isEmpty())
            value = values.get(0);
        return value;
    }

    private String splitPath(String pathWithParams){
        int paramStartIndex=pathWithParams.indexOf("?");
        if(paramStartIndex!=-1)
            return pathWithParams.substring(0,paramStartIndex);
        return pathWithParams;
    }

    private void inflaterParams(String pathWithParams) throws UnsupportedEncodingException {
        int paramStartIndex=pathWithParams.indexOf("?");
        if(paramStartIndex!=-1&&paramStartIndex<pathWithParams.length()-1){
            String paramsStr=pathWithParams.substring(paramStartIndex+1);
            String[] splitParam=paramsStr.split("&");
            for(String paramPair:splitParam){
                int assignIndex=paramPair.indexOf("=");
                if(assignIndex!=-1) {
                    String key= URLDecoder.decode(paramPair.substring(0,assignIndex),"utf-8");
                    String value=URLDecoder.decode(paramPair.substring(assignIndex+1),"utf-8");
                    List<String> list=params.get(key);
                    if(list==null) {
                        list = new ArrayList<>();
                        params.put(key,list);
                    }
                    list.add(value);
                }
            }
        }
    }

    private void parseBody(Context context,InputStream in)throws IOException{
        String contentLength=getFirstHeader(HeaderDefinition.KEY_CONTENT_LENGTH);
        int length=0;
        if(!TextUtils.isEmpty(contentLength)){
            try{
                length=Integer.parseInt(contentLength.trim());
            }catch (NumberFormatException e){
                throw new HttpClientRequestParserException("Content-Length转换异常:"+contentLength);
            }
        }

        String contentType=getFirstHeader("content-type");
        if(contentType!=null){
            List<String> contentTypeList=Arrays.asList(contentType.split(";"));
            if(contentTypeList.contains("application/x-www-form-urlencoded")){
                byte[] bodyBytes=new byte[length];
                in.read(bodyBytes);
                String bodyStr=new String(bodyBytes);
                String[] bodySplit=bodyStr.split("&");
                for(String pair:bodySplit){
                    String[] keyWithValue=pair.split("=");
                    if(keyWithValue.length>=2){
                        String key=URLDecoder.decode(keyWithValue[0],"utf-8");
                        String value=URLDecoder.decode(keyWithValue[1],"utf-8");
                        List<String> values=getParam(key);
                        values.add(value);
                    }
                }
            }
            else if(contentTypeList.contains("multipart/form-data")){
                String boundary=null;
                final String boundaryFlag="boundary=";
                for(String ct:contentTypeList){
                    if(ct.contains(boundaryFlag)){
                        boundary=ct.substring(ct.indexOf(boundaryFlag)+boundaryFlag.length());
                        break;
                    }
                }
                //如果没有boundary 清空输入流
                if(boundary==null){
                    in.skip(length);
                    throw new HttpClientRequestParserException("multipart/form-data模式，但是boundary为空");
                }
                FormData formData;
                do{
                    formData=nextFormData(context,in,boundary);
                    if(formData.strData!=null)
                        addParam(formData.name,formData.strData);
                    else if(formData.fileData!=null)
                        files.put(formData.name,Pair.create(formData.fileName,formData.fileData));
                }
                while(!formData.isLast);
            }
        }
    }

    private FormData nextFormData(Context context,InputStream in, String boundary)throws IOException{
        final String startBoundary="--"+boundary;
        final String lastBoundary="--"+boundary+"--";
        FormData formData=new FormData();

        String firstLine=readLine(in);
        if(startBoundary.equals(firstLine))
            firstLine=readLine(in);
        else if(lastBoundary.equals(firstLine)) {
            formData.isLast = true;
            return formData;
        }

        String[] contentDisposition= firstLine.split(";");
        for(String cd:contentDisposition){
            if(cd.trim().startsWith("name="))
                formData.name=cd.substring(7,cd.length()-1);
            else if(cd.trim().startsWith("filename="))
                formData.fileName=cd.substring(11,cd.length()-1);
        }
        if(formData.fileName!=null){
            String contentType=readLine(in);
            if(contentType.toLowerCase().startsWith("content-type:"))
                formData.contentType=contentType.substring(13);
        }
        //丢弃一个空行
        readLine(in);

        if(formData.fileName==null){
            formData.strData=readLine(in);
            String nextLine=readLine(in);
            if(lastBoundary.equals(nextLine))
                formData.isLast=true;
        }
        else{
            File tempFile=new File(context.getCacheDir(),Long.toString(System.currentTimeMillis()));
            OutputStream out=new BufferedOutputStream(new FileOutputStream(tempFile));
            byte[] startBoundaryBytes=(startBoundary+"\r").getBytes();
            byte[] endBoundaryBytes=(lastBoundary+"\r").getBytes();
            byte[] buffer=new byte[4096];
            int end=0;
            int c;

            while((c=in.read())!=-1){
                if(c=='\n'){
                    if(end>=startBoundaryBytes.length){
                        int equalBoundaryLength=startBoundaryBytes.length+2;
                        for(int i=1;i<=startBoundaryBytes.length;i++){
                            if(buffer[end-i]!=startBoundaryBytes[startBoundaryBytes.length-i]){
                                equalBoundaryLength=-1;
                                break;
                            }
                        }
                        if(equalBoundaryLength<0&&end>=endBoundaryBytes.length){
                            equalBoundaryLength=endBoundaryBytes.length+2;
                            formData.isLast=true;
                            for(int i=1;i<endBoundaryBytes.length;i++){
                                if(buffer[end-i]!=endBoundaryBytes[endBoundaryBytes.length-i]){
                                    equalBoundaryLength=-1;
                                    formData.isLast=false;
                                    break;
                                }
                            }
                        }
                        if(equalBoundaryLength>0) {
                            end -= equalBoundaryLength;
                            break;
                        }
                    }
                }
                buffer[end++]= (byte) c;
                if(end>=buffer.length) {
                    out.write(buffer,0,buffer.length-endBoundaryBytes.length);
                    System.arraycopy(buffer,buffer.length-endBoundaryBytes.length,buffer,0,endBoundaryBytes.length);
                    end=endBoundaryBytes.length;
                }
            }
            out.write(buffer,0,end);
            out.flush();
            out.close();
            formData.fileData=tempFile;
        }
        return formData;
    }

    private List<String> getParam(String key){
        List<String> values=params.get(key);
        if(values==null){
            values=new ArrayList<>();
            params.put(key,values);
        }
        return values;
    }

    private void addParam(String key,String value){
        List<String> values=getParam(key);
        values.add(value);
    }

    public String getParamFirst(String key){
        List<String> values=params.get(key);
        if(values!=null&&!values.isEmpty())
            return values.get(0);
        return null;
    }

    public String getPath() {
        return path;
    }

    public String getHost() {
        return host;
    }

    public String getProtocol() {
        return protocol;
    }

    public Map<String, List<String>> getHeader() {
        return header;
    }

    public Map<String,Pair<String,File>> getFiles(){
        return files;
    }
}