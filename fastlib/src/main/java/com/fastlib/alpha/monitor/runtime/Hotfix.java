package com.fastlib.alpha.monitor.runtime;

import com.fastlib.utils.ContextHolder;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

import dalvik.system.DexClassLoader;

public class Hotfix {

    private Hotfix(){}

    public static void launcher(){
        File patchFile=new File(ContextHolder.getContext().getFilesDir(),"patch.jar");
        if(patchFile.exists()&&patchFile.length()>0){
            try {
                patchUp(ContextHolder.getContext().getClassLoader(),patchFile.getAbsolutePath(),ContextHolder.getContext().getCacheDir().getAbsolutePath());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 将补丁打入系统
     * @param patchPath 补丁路径
     */
    private static void patchUp(ClassLoader classLoader,String patchPath,String cacheDirPath) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        //声明出所有需要的类型
        ClassLoader parentClassLoader=classLoader;
        DexClassLoader dexClassLoader=new DexClassLoader(patchPath,cacheDirPath,null,parentClassLoader);
        Class baseClassLoader=parentClassLoader.loadClass("dalvik.system.BaseDexClassLoader");
        Class dexClassLoaderSuperClass=dexClassLoader.getClass().getSuperclass();

        Field pathListField=baseClassLoader.getDeclaredField("pathList");
        pathListField.setAccessible(true);
        Object pathList=pathListField.get(parentClassLoader);

        Field patchPathListField=dexClassLoaderSuperClass.getDeclaredField("pathList");
        patchPathListField.setAccessible(true);
        Object patchPathList=patchPathListField.get(dexClassLoader);

        Field dexElementsField=pathList.getClass().getDeclaredField("dexElements");
        dexElementsField.setAccessible(true);
        Object[] dexElements= (Object[]) dexElementsField.get(pathList);

        Field patchDexElementsField=patchPathList.getClass().getDeclaredField("dexElements");
        patchDexElementsField.setAccessible(true);
        Object[] patchDexElements= (Object[]) patchDexElementsField.get(patchPathList);

        //组合系统类和补丁
        Object[] combinationDexElements= (Object[]) Array.newInstance(dexElements[0].getClass(),dexElements.length+patchDexElements.length);

        System.arraycopy(patchDexElements, 0, combinationDexElements, 0, patchDexElements.length);
        for(int i=patchDexElements.length;i<combinationDexElements.length;i++)
            combinationDexElements[i]=dexElements[i-patchDexElements.length];
        dexElementsField.set(pathList,combinationDexElements);
    }
}
