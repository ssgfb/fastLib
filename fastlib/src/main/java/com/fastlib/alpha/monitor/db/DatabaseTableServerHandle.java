package com.fastlib.alpha.monitor.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.fastlib.utils.ContextHolder;

import java.io.IOException;

/**
 * Created by sgfb on 2020\04\15.
 */
@Path("/database/table")
public class DatabaseTableServerHandle extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) throws IOException {
        String db=request.getParamFirst("db");
        Context context= ContextHolder.getContext();

        if(TextUtils.isEmpty(db))
            return "db或table不能为空";
        SQLiteDatabase database= SQLiteDatabase.openDatabase(context.getDatabasePath(db).getAbsolutePath(),null,SQLiteDatabase.OPEN_READONLY);
        try{
            StringBuilder sb=new StringBuilder();
            Cursor cursor=database.rawQuery("select name from sqlite_master where type='table'",null);

            sb.append("[");
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                if(sb.length()!=1)
                    sb.append(",");
                sb.append('"');
                sb.append(cursor.getString(0));
                sb.append('"');
                cursor.moveToNext();
            }
            sb.append("]");
            cursor.close();
            database.close();
            return sb.toString();
        }catch (SQLiteException ignore){
            return "数据库打开失败，请检查数据库名和表名是否输入正确";
        }
    }
}
