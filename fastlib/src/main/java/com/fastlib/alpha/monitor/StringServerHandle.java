package com.fastlib.alpha.monitor;

import com.fastlib.alpha.monitor.request.HttpClientRequest;

import java.io.IOException;

/**
 * Created by sgfb on 2020\04\17.
 * 返回Json类型交互
 */
public abstract class StringServerHandle implements ServerHandle {

    protected abstract String handle(HttpClientRequest request)throws IOException;

    @Override
    public HttpResponse interaction(HttpClientRequest request) throws IOException {
        String result=handle(request);
        HttpResponse response=new HttpResponse();
        if(result!=null){
            response.addBody(result);
        }
        return response;
    }
}
