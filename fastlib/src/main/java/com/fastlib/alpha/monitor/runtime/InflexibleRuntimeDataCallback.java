package com.fastlib.alpha.monitor.runtime;

/**
 * Created by sgfb on 2020\04\15.
 * 不可变更运行时交互回调
 */
public abstract class InflexibleRuntimeDataCallback implements RuntimeDataCallback{

    @Override
    public boolean canChange() {
        return false;
    }

    @Override
    public void changeTo(String newValue) {
        throw new InjectReplaceRuntimeDataException();
    }
}
