package com.fastlib.alpha.monitor.runtime;

import android.app.ActivityManager;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

/**
 * Created by sgfb on 2020\04\20.
 * 手机属性
 */
public class PhoneProfile{
    public static final String TAG=PhoneProfile.class.getSimpleName();
    private boolean wifiConnected;
    private boolean charging;
    private int batteryRemain;
    private int cpuCoreCount;
    private int totalMemory;     //手机系统内存大小.MB
    private int freeMemory;      //当前系统可用内存.MB
    private int totalStorage;    //当前手机外存大小.MB
    private int freeStorage;     //当前手机可用外存.MB
    private int androidSDKVersion;
    private String brand;

    public PhoneProfile(Context context){
        if(Build.VERSION.SDK_INT>=23){
            BatteryManager batteryManager= (BatteryManager) context.getSystemService(Context.BATTERY_SERVICE);
            charging=batteryManager.isCharging();
            batteryRemain=batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        }
        try{
            WifiManager wifiManager= (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            wifiConnected=wifiManager.isWifiEnabled();
        }catch (SecurityException ignore){
            Log.w(TAG,"读取wifi状态时无权限");
        }

        ActivityManager activityManager= (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo=new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        totalMemory= (int) (memoryInfo.totalMem/1024/1024);
        freeMemory= (int) (memoryInfo.availMem/1024/1024);

        StatFs statFs=new StatFs(Environment.getDataDirectory().getPath());
        if(Build.VERSION.SDK_INT>=18){
            totalStorage= (int) (statFs.getTotalBytes()/1024/1024);
            freeStorage= (int) (statFs.getFreeBytes()/1024/1024);
        }

        cpuCoreCount=Runtime.getRuntime().availableProcessors();
        androidSDKVersion=Build.VERSION.SDK_INT;
        brand=Build.MODEL;
    }

    public boolean isCharging() {
        return charging;
    }

    public boolean isWifiConnected() {
        return wifiConnected;
    }

    public int getCpuCoreCount() {
        return cpuCoreCount;
    }

    public int getTotalMemory() {
        return totalMemory;
    }

    public int getTotalStorage() {
        return totalStorage;
    }

    public int getFreeStorage() {
        return freeStorage;
    }

    public int getAndroidSDKVersion() {
        return androidSDKVersion;
    }

    public String getBrand() {
        return brand;
    }

    public int getBatteryRemain() {
        return batteryRemain;
    }

    public int getFreeMemory() {
        return freeMemory;
    }
}
