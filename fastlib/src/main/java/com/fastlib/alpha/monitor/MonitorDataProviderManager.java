package com.fastlib.alpha.monitor;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;

/**
 * Created by sgfb on 2020\04\13.
 */
public final class MonitorDataProviderManager{
    private static Map<String, ServerHandle> sMonitorDataProviderMap=new HashMap<>();

    private MonitorDataProviderManager(){}

    public static @NonNull
    ServerHandle getHandler(String path){
        ServerHandle dataServerHandle =sMonitorDataProviderMap.get(path);
        if(dataServerHandle !=null)
            return dataServerHandle;
        return new PathNotFoundServerHandle();
    }

    public static void putMonitorDataProvider(ServerHandle serverHandle){
        Path path= serverHandle.getClass().getAnnotation(Path.class);
        if(path==null)
            throw new IllegalArgumentException("MonitorDataProvider需要注解路径Path:"+ serverHandle.getClass());
        sMonitorDataProviderMap.put(path.value(), serverHandle);
    }
}
