package com.fastlib.alpha.monitor;

import java.io.IOException;

/**
 * Created by sgfb on 2020\04\13.
 * 解析客户端传入请求时异常
 */
public class HttpClientRequestParserException extends IOException{

    public HttpClientRequestParserException() {
    }

    public HttpClientRequestParserException(String message) {
        super(message);
    }

    public HttpClientRequestParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpClientRequestParserException(Throwable cause) {
        super(cause);
    }
}
