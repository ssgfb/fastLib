package com.fastlib.alpha.monitor.file;

import android.os.Environment;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sgfb on 2020\04\15.
 * 返回指定路径文件列表
 */
@Path("/file/list")
public class FileListServerHandle extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) throws IOException {
        String path=request.getParamFirst("path");
        if(path==null)
            path= Environment.getExternalStorageDirectory().getAbsolutePath();

        File file=new File(path);
        if(!file.exists())
            return "路径不存在";

        List<FileInfo> fileList=new ArrayList<>();

        if(file.isFile())
            fileList.add(new FileInfo(file.isDirectory(),file.length(),file.lastModified(),file.getName()));
        else{
            for(File child:file.listFiles())
                fileList.add(new FileInfo(child.isDirectory(),child.length(),child.lastModified(),child.getName()));
        }
        return new Gson().toJson(fileList);
    }
}
