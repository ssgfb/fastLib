package com.fastlib.alpha.monitor.runtime;

/**
 * Created by sgfb on 2020\04\15.
 * 运行时数据交互回调
 */
public interface RuntimeDataCallback {

    /**
     * 返回运行时数据以字符串形式显示
     */
    String showData();

    /**
     * 是否可修改
     */
    boolean canChange();

    /**
     * 修改运行时数据
     */
    void changeTo(String newValue);
}
