package com.fastlib.alpha.monitor.db;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.fastlib.utils.ContextHolder;

import java.io.File;
import java.io.IOException;

/**
 * Created by sgfb on 2020\04\15.
 */
@Path("/sp/list")
public class SpServerHandle extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) throws IOException {
        File director=new File(ContextHolder.getContext().getFilesDir().getParent(),"shared_prefs");

        StringBuilder sb=new StringBuilder();
        sb.append("[");
        for(String child:director.list()){
            if(sb.length()!=1)
                sb.append(",");
            sb.append('"');
            sb.append(child);
            sb.append('"');
        }
        sb.append("]");
        return sb.toString();
    }
}
