package com.fastlib.alpha.monitor.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.fastlib.utils.ContextHolder;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sgfb on 2020\04\13.
 */
@Path("/database")
public class DatabaseDBServerHandle extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) {
        Context context= ContextHolder.getContext();
        String[] dbList=context.databaseList();
        List<String> canOpenDbs=new ArrayList<>();
        for(String db:dbList){
            try(SQLiteDatabase sqliteDatabase=SQLiteDatabase.openDatabase(context.getDatabasePath(db).getAbsolutePath(),null,SQLiteDatabase.OPEN_READONLY)){
                Cursor cursor=sqliteDatabase.rawQuery("select name from sqlite_master where type='table' limit 0,1",null);
                cursor.close();
                sqliteDatabase.close();
                canOpenDbs.add(db);
            }catch (SQLiteException ignore){}
        }
        Gson gson=new Gson();
        return gson.toJson(canOpenDbs);
    }
}