package com.fastlib.alpha.monitor.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.fastlib.utils.ContextHolder;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by sgfb on 2020\04\15.
 * 输出指定数据库指定表中数据
 */
@Path("/database/data")
public class DatabaseColumnServerHandle extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) {
        String db=request.getParamFirst("db");
        String table=request.getParamFirst("table");
        Context context= ContextHolder.getContext();

        if(TextUtils.isEmpty(db)||TextUtils.isEmpty(table))
            return "db和table不能为空";
        try(SQLiteDatabase database=SQLiteDatabase.openDatabase(context.getDatabasePath(db).getAbsolutePath(),null,SQLiteDatabase.OPEN_READONLY)){
            List<Map<String,String>> list=new ArrayList<>();
            Cursor cursor=database.rawQuery(String.format(Locale.getDefault(),"select *from '%s' limit 0,100",table),null);

            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                Map<String,String> map=new HashMap<>();
                for(int i=0;i<cursor.getColumnCount();i++){
                    map.put(cursor.getColumnName(i),cursor.getString(i));
                }
                list.add(map);
                cursor.moveToNext();
            }

            cursor.close();
            Gson gson=new Gson();
            return gson.toJson(list);
        }catch (SQLiteException e){
            return "打开数据库异常:"+e.getMessage();
        }
    }
}
