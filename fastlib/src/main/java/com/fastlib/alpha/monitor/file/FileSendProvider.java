package com.fastlib.alpha.monitor.file;

import android.text.TextUtils;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.alpha.monitor.HttpResponse;
import com.fastlib.alpha.monitor.ServerHandle;
import com.fastlib.alpha.monitor.Path;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * Created by sgfb on 2020\04\15.
 * 返回指定路径的文件原始字节(客户端下载)
 */
@Path("/file/download")
public class FileSendProvider implements ServerHandle {

    @Override
    public HttpResponse interaction(HttpClientRequest request) throws IOException {
        String path=request.getParamFirst("path");
        String errorMessage=checkFilePath(path);

        HttpResponse response=new HttpResponse();
        if(errorMessage!=null){
            response.addBody(errorMessage);
        }
        else{
            File file=new File(path);
            response.addHeader("Content-Type","application/octet-stream");
            response.addHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode(file.getName(),"UTF-8"));
            response.addHeader("Content-Type","application/octet-stream");
            response.addBody(new FileInputStream(file));
        }
        return response;
    }

    /**
     * 检查文件路径
     * @param path  文件路径
     * @return  如果文件正常返回空，否则返回错误信息
     */
    private String checkFilePath(String path){
        if(TextUtils.isEmpty(path))
            return "path不能为空";
        File file=new File(path);
        if(!file.exists())
            return "路径文件不存在";
        if(file.isDirectory())
            return "此路径为文件夹,不是文件";
        return null;
    }
}
