package com.fastlib.alpha.monitor.runtime;

/**
 * Created by sgfb on 2020\04\15.
 * 试图修改不可变更数据时抛出此异常
 */
public class InjectReplaceRuntimeDataException extends RuntimeException{

    public InjectReplaceRuntimeDataException() {
        super();
    }

    public InjectReplaceRuntimeDataException(String message) {
        super(message);
    }

    public InjectReplaceRuntimeDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public InjectReplaceRuntimeDataException(Throwable cause) {
        super(cause);
    }
}
