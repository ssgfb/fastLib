package com.fastlib.alpha.monitor.runtime;

import android.content.Context;

/**
 * Created by sgfb on 2020\04\20.
 * 宿主应用信息
 */
public class AppProfile{
    private int totalMemory;    //jvm内分配总大小.KB单位
    private int maxMemory;      //jvm最大可分配.KB单位
    private int freeMemory;     //jvm当前可用空间.KB单位
    private String packageName;

    public AppProfile(Context context){
        Runtime runtime=Runtime.getRuntime();
        totalMemory= (int) (runtime.totalMemory()/1024);
        maxMemory= (int) (runtime.maxMemory()/1024);
        freeMemory= (int) (runtime.freeMemory()/1024);
        packageName=context.getPackageName();
    }

    public int getTotalMemory() {
        return totalMemory;
    }

    public int getMaxMemory() {
        return maxMemory;
    }

    public int getFreeMemory() {
        return freeMemory;
    }

    public String getPackageName() {
        return packageName;
    }
}
