package com.fastlib.alpha.monitor.db;

import android.text.TextUtils;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.fastlib.db.SaveUtil;
import com.fastlib.utils.ContextHolder;

import java.io.File;
import java.io.IOException;

/**
 * Created by sgfb on 2020\04\15.
 */
@Path("/sp/data")
public class SpDataServerHandle extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) throws IOException {
        String name=request.getParamFirst("name");
        if(TextUtils.isEmpty(name))
            return "name不能为空";

        File parent=new File(ContextHolder.getContext().getFilesDir().getParent(),"shared_prefs");
        File file=new File(parent,name);

        if(!file.exists())
            return "文件不存在，请检查name字段是否正确";

        return new String(SaveUtil.loadFile(file.getAbsolutePath()));
    }
}
