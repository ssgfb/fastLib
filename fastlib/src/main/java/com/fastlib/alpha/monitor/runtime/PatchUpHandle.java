package com.fastlib.alpha.monitor.runtime;

import com.fastlib.alpha.monitor.*;
import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.utils.ContextHolder;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

@Path("/runtime/patchUp")
public class PatchUpHandle extends JsonServerHandle {

    @Override
    protected Object handleResult(HttpClientRequest request) throws IOException{
        ServerHandle fileReceiver=  MonitorDataProviderManager.getHandler("/file/upload");
        try {
            Method method=HttpClientRequest.class.getDeclaredMethod("getParam",String.class);
            method.setAccessible(true);
            List<String> requestParams= (List<String>) method.invoke(request,"path");
            requestParams.add(ContextHolder.getContext().getFilesDir().getAbsolutePath());
            fileReceiver.interaction(request);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    System.exit(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
        return "打入补丁成功";
    }
}
