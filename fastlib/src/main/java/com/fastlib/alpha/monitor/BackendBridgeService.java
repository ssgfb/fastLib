package com.fastlib.alpha.monitor;

import android.content.Intent;
import android.util.Log;

import com.fastlib.alpha.monitor.db.DatabaseColumnServerHandle;
import com.fastlib.alpha.monitor.db.DatabaseDBServerHandle;
import com.fastlib.alpha.monitor.db.DatabasePageProvider;
import com.fastlib.alpha.monitor.db.DatabaseTableServerHandle;
import com.fastlib.alpha.monitor.db.SpDataServerHandle;
import com.fastlib.alpha.monitor.db.SpServerHandle;
import com.fastlib.alpha.monitor.file.FileManagerPageProvider;
import com.fastlib.alpha.monitor.file.FileReceiveProvider;
import com.fastlib.alpha.monitor.file.FileSendProvider;
import com.fastlib.alpha.monitor.file.FileListServerHandle;
import com.fastlib.alpha.monitor.runtime.AppProfileServerHandle;
import com.fastlib.alpha.monitor.runtime.PatchUpHandle;
import com.fastlib.alpha.monitor.runtime.PhoneProfileServerHandle;
import com.fastlib.alpha.monitor.runtime.RuntimeDataServerHandle;
import com.fastlib.alpha.monitor.request.HttpClientRequest;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import androidx.core.util.Pair;

/**
 * Created by sgfb on 2020\04\13.
 * 提供ServiceSocket对接外部段扩交互
 */
public class BackendBridgeService extends AsyncCommandService {
    private static final String TAG= BackendBridgeService.class.getSimpleName();

    public static final String ARG_INT_COMMAND="command";
    public static final int COMMAND_START=1;
    public static final int COMMAND_STOP=2;

    private static final int PORT = 3888;
    private ThreadPoolExecutor mExecutor= new ThreadPoolExecutor(4,200,10, TimeUnit.MINUTES,new LinkedBlockingDeque<>());
    private ServerSocket mServer;

    @Override
    public void onCreate() {
        super.onCreate();
        MonitorDataProviderManager.putMonitorDataProvider(new IndexServerHandle());
        MonitorDataProviderManager.putMonitorDataProvider(new DatabaseDBServerHandle());
        MonitorDataProviderManager.putMonitorDataProvider(new DatabaseTableServerHandle());
        MonitorDataProviderManager.putMonitorDataProvider(new DatabaseColumnServerHandle());
        MonitorDataProviderManager.putMonitorDataProvider(new SpServerHandle());
        MonitorDataProviderManager.putMonitorDataProvider(new SpDataServerHandle());
        MonitorDataProviderManager.putMonitorDataProvider(new FileListServerHandle());
        MonitorDataProviderManager.putMonitorDataProvider(new FileSendProvider());
        MonitorDataProviderManager.putMonitorDataProvider(new FileReceiveProvider());
        MonitorDataProviderManager.putMonitorDataProvider(new RuntimeDataServerHandle());
        MonitorDataProviderManager.putMonitorDataProvider(new DatabasePageProvider());
        MonitorDataProviderManager.putMonitorDataProvider(new PhoneProfileServerHandle());
        MonitorDataProviderManager.putMonitorDataProvider(new AppProfileServerHandle());
        MonitorDataProviderManager.putMonitorDataProvider(new FileManagerPageProvider());
        MonitorDataProviderManager.putMonitorDataProvider(new PatchUpHandle());
    }

    @Override
    protected void startCommand(Intent intent, int flags, int startId){
        if(intent==null)
            return;

        int command=intent.getIntExtra(ARG_INT_COMMAND,-1);

        switch (command){
            case COMMAND_START:
                if(mServer==null||mServer.isClosed()){
                    synchronized (this){
                        try {
                            mServer=new ServerSocket(PORT);
                            Log.d(TAG,String.format(Locale.getDefault(),"Server(%d)启动成功",PORT));
                            startClientListener();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else
                    Log.d(TAG,"Server正在运行中");
                break;
            case COMMAND_STOP:
                if(mServer!=null&&!mServer.isClosed()){
                    synchronized (this){
                        try {
                            mServer.close();
                            Log.d(TAG,"Server关闭成功");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else
                    Log.d(TAG,"Server未在运行中");
                break;
            default:
                Log.w(TAG,String.format(Locale.getDefault(),"无法理解的命令:%d",command));
                break;
        }
    }

    private void startClientListener(){
        mExecutor.execute(()->{
            while(mServer!=null&&!mServer.isClosed()){
                try {
                    Socket socket=mServer.accept();
                    clientSession(socket);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void clientSession(Socket socket){
        Log.d(TAG,String.format(Locale.getDefault(),"开始处理来自%s的请求",socket.getRemoteSocketAddress()));
        mExecutor.execute(()->{
            try{
                HttpClientRequest request = HttpClientRequest.parserClientRequest(BackendBridgeService.this,socket.getInputStream());
                OutputStream out=socket.getOutputStream();
                ServerHandle handler= MonitorDataProviderManager.getHandler(request.getPath());
                HttpResponse response=handler.interaction(request);
                response.serverResponse(out);
                socket.close();
                for(Map.Entry<String,Pair<String,File>> entry:request.getFiles().entrySet()){
                    entry.getValue().second.delete();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                try {
                    socket.close();
                } catch (IOException ignore){}
            }
        });
    }
}
