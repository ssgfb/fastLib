package com.fastlib.alpha.monitor.db;

import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.fastlib.db.SaveUtil;
import com.fastlib.utils.ContextHolder;

import java.io.IOException;

/**
 * Created by sgfb on 2020\04\16.
 */
@Path("/database/index")
public class DatabasePageProvider extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request)throws IOException {
        return new String(SaveUtil.loadInputStream(ContextHolder.getContext().getAssets().open("DatabaseManager.html"),true));
    }
}
