package com.fastlib.alpha.monitor.request;

import java.io.File;

/**
 * Created by sgfb on 2020\04\19.
 */
public class FormData{
    public boolean isLast;
    public String name;
    public String fileName;
    public String contentType;
    public String strData;
    public File fileData;
}
