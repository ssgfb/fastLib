package com.fastlib.alpha.monitor.runtime;

import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.utils.ContextHolder;
import com.google.gson.Gson;

import java.io.IOException;

/**
 * Created by sgfb on 2020\04\20.
 * 手机属性读取
 */
@Path("/runtime/phone_profile")
public class PhoneProfileServerHandle extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) throws IOException{
        PhoneProfile pp=new PhoneProfile(ContextHolder.getContext());
        Gson gson=new Gson();
        return gson.toJson(pp);
    }
}
