package com.fastlib.alpha.monitor.runtime;

import com.fastlib.alpha.monitor.StringServerHandle;
import com.fastlib.alpha.monitor.Path;
import com.fastlib.alpha.monitor.request.HttpClientRequest;
import com.fastlib.utils.ContextHolder;
import com.google.gson.Gson;

import java.io.IOException;

/**
 * Created by sgfb on 2020\04\20.
 */
@Path("/runtime/app_profile")
public class AppProfileServerHandle extends StringServerHandle {

    @Override
    protected String handle(HttpClientRequest request) throws IOException {
        AppProfile ap=new AppProfile(ContextHolder.getContext());
        Gson gson=new Gson();
        return gson.toJson(ap);
    }
}
