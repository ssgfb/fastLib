package com.fastlib.db.condition_utils;

import com.fastlib.app.ThreadPoolManager;

import java.util.List;

/**
 * Created by sgfb on 2020\03\15.
 * 条件存储器.启动后将无限间隔{@link #mTimerCondition}循环直至{@link #mRunning}为false
 * @param <T>  存什么
 */
public abstract class ConditionPersisted<T> implements Runnable{
    private long mTimerCondition=5000;
    private boolean mRunning;
    private DataSyncChecker<T> mDataSyncChecker;

    /**
     * 读取存在外部的数据
     */
    protected abstract List<T> loadExternalDataList();

    /**
     * 怎么存
     */
    protected abstract void onPersist(List<ChangedData<T>> changedList);

    public ConditionPersisted(){
        mRunning=true;
        mDataSyncChecker =new DataSyncChecker<>(this::loadExternalDataList);
        ThreadPoolManager.getWorkHandler().postDelayed(this,mTimerCondition);
    }

    @Override
    public void run(){
        if(mRunning)
            ThreadPoolManager.getWorkHandler().postDelayed(this,mTimerCondition);
        List<ChangedData<T>> needSyncList= mDataSyncChecker.syncData();
        System.out.println("触发时间条件,"+(needSyncList==null||needSyncList.isEmpty()?"数据没有变动":"数据变动执行存储"));
        if(needSyncList!=null&&!needSyncList.isEmpty()){
            onPersist(needSyncList);
            mDataSyncChecker.refresh();
        }
    }

    public void stop(){
        mRunning=false;
    }

    public boolean isRunning(){
        return mRunning;
    }

    public void addData(T data){
        mDataSyncChecker.add(data);
    }

    public void deleteData(T data){
        mDataSyncChecker.delete(data);
    }

    public void deleteData(int index){
        List<T> internalList= mDataSyncChecker.getInternalDataList();
        if(index>=internalList.size())
            throw new IllegalArgumentException("指定的index超出范围:指定"+index+" 最大:"+(internalList.size()-1));
        mDataSyncChecker.delete(internalList.get(index));
    }

    public void valueChanged(T oldData,T newData){
        mDataSyncChecker.valueChanged(oldData,newData);
    }

    public void valueChanged(T value){
        valueChanged(value,value);
    }

    public List<T> getInternalData(){
        return mDataSyncChecker.getInternalDataList();
    }
}
