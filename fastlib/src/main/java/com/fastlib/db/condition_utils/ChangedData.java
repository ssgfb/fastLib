package com.fastlib.db.condition_utils;

import androidx.annotation.Nullable;

/**
 * Created by sgfb on 2020\03\15.
 * 标记被修改数据属性
 */
public class ChangedData<T>{
    public static final int TYPE_ADD =1;
    public static final int TYPE_DELETE =2;
    public static final int TYPE_VALUE_CHANGED=3;

    public int position;
    public int changeType;
    public T data;

    public ChangedData(int position, int changeType, T data) {
        this.position = position;
        this.changeType = changeType;
        this.data = data;
    }

    @Override
    public boolean equals(@Nullable Object obj){
        if(obj instanceof ChangedData){
            ChangedData other= (ChangedData) obj;
            return position==other.position&&changeType==other.changeType&&
                    (data==other.data||(data!=null&&data.equals(other.data)));
        }
        return false;
    }
}
