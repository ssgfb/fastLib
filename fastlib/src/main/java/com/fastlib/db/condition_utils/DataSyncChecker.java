package com.fastlib.db.condition_utils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

/**
 * Created by sgfb on 2020\03\15.
 * 数据一致性检查器.保存新旧数据以判断是否两边同步
 */
public class DataSyncChecker<T>{
    /**
     * 外部数据指的是与外部存储上的数据同步的,定时刷新同步的
     * 而内部数据是随机刷新的.先通过内外数据同步最后刷新到外部存储
     */
    private List<T> mExternalDataList;
    private List<T> mInternalDataList;
    private ExternalLoader<T> mLoader;
    private boolean isChanged;          //任何数据变动事件都触发这个标识

    public DataSyncChecker(@NonNull ExternalLoader<T> loader){
        mLoader=loader;
        mExternalDataList=new ArrayList<>();
        mInternalDataList=new ArrayList<>();

        List<T> externalData=mLoader.onLoadExternalData();
        if(externalData==null) externalData=new ArrayList<>();
        refresh(externalData);
    }

    private void refresh(List<T> latestExternalData){
        mExternalDataList.clear();
        mInternalDataList.clear();
        mExternalDataList.addAll(latestExternalData);
        mInternalDataList.addAll(latestExternalData);
    }

    public void refresh(){
        refresh(mLoader.onLoadExternalData());
    }

    public void add(T data){
        int index= mInternalDataList.indexOf(data);

        if(index==-1)
            mInternalDataList.add(data);
        isChanged=true;
    }

    public void delete(T data){
        int index=mInternalDataList.indexOf(data);

        if(index!=-1)
            mInternalDataList.set(index,null);
        isChanged=true;
    }

    public void valueChanged(T oldData, T newData){
        int index=mInternalDataList.indexOf(oldData);

        if(index!=-1)
            mInternalDataList.set(index,newData);
        isChanged=true;
    }

    /**
     * 同步内外数据并得出不同数据列表
     * @return  一个与外部数据等长列表,未改变的数据项用null表示
     */
    public List<ChangedData<T>> syncData(){
        if(!isChanged) return null;

        List<ChangedData<T>> needSyncList=new ArrayList<>();

        //填充变更和删除数据
        for(int i=0;i<mExternalDataList.size();i++){
            T externalData=mExternalDataList.get(i);
            T internalData=mInternalDataList.get(i);

            int type=0;
            T data=null;
            if(internalData==null) {
                type = ChangedData.TYPE_DELETE;
                data=externalData;
            }
            else if(!externalData.equals(internalData)) {
                type = ChangedData.TYPE_VALUE_CHANGED;
                data=internalData;
            }
            if(type!=0)
                needSyncList.add(new ChangedData<>(i,type,data));
        }
        for(int i=mExternalDataList.size();i<mInternalDataList.size();i++){
            T newElement=mInternalDataList.get(i);
            needSyncList.add(new ChangedData<>(i,ChangedData.TYPE_ADD,newElement));
        }
        return needSyncList;
    }

    @VisibleForTesting
    public List<T> getExternalDataList(){
        return mExternalDataList;
    }

    public List<T> getInternalDataList(){
        return mInternalDataList;
    }
}
