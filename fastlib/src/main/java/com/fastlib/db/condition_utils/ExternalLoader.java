package com.fastlib.db.condition_utils;

import java.util.List;

/**
 * Created by sgfb on 2020\03\15.
 */
public interface ExternalLoader<T>{

    List<T> onLoadExternalData();
}
