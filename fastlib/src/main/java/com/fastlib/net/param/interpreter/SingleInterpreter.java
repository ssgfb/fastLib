package com.fastlib.net.param.interpreter;

import com.fastlib.net.param.RequestParam;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;

/**
 * Created by sgfb on 2019\12\24.
 */
public abstract class SingleInterpreter implements ParamInterpreter{

    protected abstract InputStream interpreterAdapter(RequestParam param);

    @Override
    public List<InputStream> interpreter(RequestParam param,@NonNull List<ValuePosition> valuePositions) {
        List<InputStream> list=new ArrayList<>();
        list.add(interpreterAdapter(param));
        return list;
    }
}
