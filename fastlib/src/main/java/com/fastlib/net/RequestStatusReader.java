package com.fastlib.net;

import androidx.annotation.StringDef;

/**
 * Created by sgfb on 2020\03\13.
 * {@link Request}状态读取器
 */
public interface RequestStatusReader{
    String STATUS_READY="ready";            //已准备未开始网络请求
    String STATUS_CONNECTING="connecting";  //已开始流程
    String STATUS_RUNNING="running";        //已连接,正在互相传输数据
    String STATUS_COMPLETED="completed";    //完成(错误结束也是完成的一种)
    String STATUS_CANCELED="canceled";      //手动取消

    @StringDef({
            STATUS_READY,
            STATUS_CONNECTING,
            STATUS_RUNNING,
            STATUS_COMPLETED,
            STATUS_CANCELED
    })
    @interface RequestStatus{}

    /**
     * 读取网络请求的当前状态
     */
    @RequestStatus String readStatus();
}
