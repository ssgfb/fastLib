package com.fastlib.demo;

import android.Manifest;
import com.fastlib.alpha.precondition.EnvGetter;
import com.fastlib.alpha.precondition.Permission;

public class ProxyObj {

    @Permission(Manifest.permission.CAMERA)
    public void doSomething(EnvGetter getter){
        System.out.println("camera permission got");
    }
}
