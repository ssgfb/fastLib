package com.fastlib.demo.net;

import com.fastlib.alpha.file_transmission.DownloadFileService;
import com.fastlib.annotation.Bind;
import com.fastlib.annotation.ContentView;
import com.fastlib.annotation.Event;
import com.fastlib.aspect.base.AspectActivity;
import com.fastlib.bean.event.EventDownloading;
import com.fastlib.bean.event.EventUploading;
import com.fastlib.demo.R;
import com.fastlib.demo.route.Empty;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sgfb on 2020\03\04.
 */
@ContentView(R.layout.act_net)
public class NetDemoActivity extends AspectActivity<Empty,Empty>{
    @Bind(R.id.list)
    RecyclerView mList;
    DownloadingAdapter mAdapter;
    Map<Integer,Boolean> mMap=new HashMap<>();

    @Override
    protected void onReady(){

    }

    @Bind(R.id.bt)
    private void request(){
        File file=new File(getExternalCacheDir(),"image.jpg");
        UploadFileService.uploadFile(this,file);
    }

    @Bind(R.id.bt2)
    private void cancelRequest(){
        File file=new File(getExternalCacheDir(),"image.jpg");
        DownloadFileService.addDownloadRequest(this,"http://192.168.3.20/logo.jpg",file.getAbsolutePath());
    }

    @Event
    private void eDownloading(EventDownloading event){
        System.out.println(event.getSpeed()+" "+new File(event.getPath()).length()+"/"+event.getMaxLength());
//        boolean replace=false;
//        for(int i=0;i<mAdapter.getData().size();i++){
//            if(mAdapter.getData().get(i).getPath().equals(event.getPath())){
//                mAdapter.getData().remove(i);
//                mAdapter.getData().add(i,event);
//                mAdapter.notifyDataSetChanged();
//                replace=true;
//                break;
//            }
//        }
//        if(!replace)
//            mAdapter.addData(event);
    }

    @Event
    private void eUploading(EventUploading event){
        File file=new File(event.getPath());
        System.out.println(event.getSendByte()+"/"+file.length());
    }
}
