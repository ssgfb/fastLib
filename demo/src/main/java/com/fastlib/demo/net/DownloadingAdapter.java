package com.fastlib.demo.net;

import android.widget.ProgressBar;

import com.fastlib.adapter.BaseRecyAdapter;
import com.fastlib.annotation.ContentView;
import com.fastlib.base.RecyclerViewHolder;
import com.fastlib.bean.event.EventDownloading;
import com.fastlib.demo.R;

import java.io.File;

/**
 * Created by sgfb on 2020\03\13.
 */
@ContentView(R.layout.item_downloading)
public class DownloadingAdapter extends BaseRecyAdapter<EventDownloading>{

    @Override
    public void binding(int position, EventDownloading data, RecyclerViewHolder holder){
        File file=new File(data.getPath());

        holder.setText(R.id.name,file.getName());
        holder.setText(R.id.speed,(data.getSpeed()/1024)+"KB");

        long currentLen=file.length();
        ((ProgressBar)holder.getView(R.id.progress)).setProgress((int) (currentLen*100/data.getMaxLength()));
    }
}
