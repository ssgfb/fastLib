package com.fastlib.demo.net;

import android.content.Context;
import android.content.Intent;

import com.fastlib.alpha.file_transmission.FileTransmissionService;
import com.fastlib.app.EventObserver;
import com.fastlib.bean.event.EventUploading;
import com.fastlib.net.Request;
import com.fastlib.net.core.Method;

import java.io.File;

/**
 * Created by sgfb on 2020\04\05.
 * 上传文件后台服务示例
 */
public class UploadFileService extends FileTransmissionService{
    private static String UPLOAD_URL="http://192.168.3.20/uploadFile";

    public static final String ARG_STR_FILEPATH="filePath";

    private String mFilePath;

    public static void uploadFile(Context context,File file){
        Intent intent=new Intent(context,UploadFileService.class);
        intent.putExtra(ARG_STR_COMMAND,COMMAND_START);
        intent.putExtra(ARG_STR_FILEPATH,file.getAbsolutePath());
        context.startService(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        mFilePath=intent.getStringExtra(ARG_STR_FILEPATH);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected String genRequestKey(){
        return mFilePath;
    }

    @Override
    protected Request genRequestByKey(String key) {
        Request request=new Request(UPLOAD_URL, Method.POST);
        request.put("file",new File(key));
        request.setUploadingListener((key1, wrote, count) ->{
            EventObserver.getInstance().sendEvent(UploadFileService.this,new EventUploading(wrote,key));
        });
        return request;
    }

    @Override
    protected void clearRequest(Request request) {

    }
}
