package com.fastlib.demo.db;

import android.os.Bundle;
import android.widget.EditText;

import com.fastlib.app.FastDialog;
import com.fastlib.db.FastDatabase;
import com.fastlib.demo.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

/**
 * Created by sgfb on 2020\03\15.
 */
public class ConditionSaveActivity extends AppCompatActivity{
    ConditionPersistedPerson mConditionPersisted;
    int mIndex=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_condition_save);

        mConditionPersisted=new ConditionPersistedPerson();
        EditText editText=findViewById(R.id.edit);
        findViewById(R.id.add).setOnClickListener(v -> {
            mConditionPersisted.addData(new Person(mIndex++,editText.getText().toString().trim()));
            editText.setText(null);
        });

        RecyclerView dbList=findViewById(R.id.dbList);
        PersonAdapter dbAdapter=new PersonAdapter();
        dbList.setAdapter(dbAdapter);
        ((SwipeRefreshLayout)findViewById(R.id.dbRefresh)).setOnRefreshListener(() -> {
            dbAdapter.setData(FastDatabase.getDefaultInstance(ConditionSaveActivity.this).get(Person.class));
            ((SwipeRefreshLayout)findViewById(R.id.dbRefresh)).setRefreshing(false);
        });

        RecyclerView memoryList=findViewById(R.id.memoryList);
        PersonAdapter memoryAdapter=new PersonAdapter();
        memoryAdapter.setOnItemClickListener((position, holder, data) -> {
            FastDialog.showListDialog(new String[]{"删除","修改"}).show(getSupportFragmentManager(), (dialog, which) -> {
                if(which==0)
                    mConditionPersisted.deleteData(position);
                else
                    mConditionPersisted.valueChanged(data);
            });
        });
        memoryList.setAdapter(memoryAdapter);
        ((SwipeRefreshLayout)findViewById(R.id.memoryRefresh)).setOnRefreshListener(() -> {
            memoryAdapter.setData(mConditionPersisted.getInternalData());
            ((SwipeRefreshLayout)findViewById(R.id.memoryRefresh)).setRefreshing(false);
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mConditionPersisted!=null) mConditionPersisted.stop();
    }
}
