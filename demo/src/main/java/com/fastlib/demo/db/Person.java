package com.fastlib.demo.db;

import com.fastlib.annotation.Database;

/**
 * Created by sgfb on 2020\03\15.
 */
public class Person{
    @Database(keyPrimary = true)
    public int id;
    public String name;

    public Person() {
    }

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
