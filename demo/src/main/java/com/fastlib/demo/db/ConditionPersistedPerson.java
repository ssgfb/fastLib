package com.fastlib.demo.db;

import com.fastlib.db.FastDatabase;
import com.fastlib.db.condition_utils.ChangedData;
import com.fastlib.db.condition_utils.ConditionPersisted;
import com.fastlib.utils.ContextHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sgfb on 2020\03\15.
 */
public class ConditionPersistedPerson extends ConditionPersisted<Person> {

    @Override
    protected List<Person> loadExternalDataList() {
        return FastDatabase.getDefaultInstance(ContextHolder.getContext()).get(Person.class);
    }

    @Override
    protected void onPersist(List<ChangedData<Person>> changedList) {
        List<Person> addList = new ArrayList<>();
        List<Person> delList = new ArrayList<>();
        List<Person> updateList = new ArrayList<>();
        for (ChangedData<Person> e : changedList) {
            switch (e.changeType) {
                case ChangedData.TYPE_ADD:
                    addList.add(e.data);
                    break;
                case ChangedData.TYPE_DELETE:
                    delList.add(e.data);
                    break;
                case ChangedData.TYPE_VALUE_CHANGED:
                    updateList.add(e.data);
                    break;
                default:
                    System.out.println("错误类型:"+e.changeType);
                    break;
            }
        }

        FastDatabase.getDefaultInstance(ContextHolder.getContext()).saveOrUpdate(addList);

        for(Person needDelPerson:delList)
            FastDatabase.getDefaultInstance(ContextHolder.getContext()).delete(needDelPerson);
        for(Person needUpdatePerson:updateList)
            FastDatabase.getDefaultInstance(ContextHolder.getContext()).update(needUpdatePerson);
    }
}
