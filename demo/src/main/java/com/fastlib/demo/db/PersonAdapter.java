package com.fastlib.demo.db;

import com.fastlib.adapter.BaseRecyAdapter;
import com.fastlib.annotation.ContentView;
import com.fastlib.base.RecyclerViewHolder;
import com.fastlib.demo.R;

import java.util.Locale;

/**
 * Created by sgfb on 2020\03\15.
 */
@ContentView(R.layout.item_db_person)
public class PersonAdapter extends BaseRecyAdapter<Person>{

    @Override
    public void binding(int position, Person data, RecyclerViewHolder holder) {
        holder.setText(R.id.text,String.format(Locale.getDefault(),"%d:%s",data.id,data.name));
    }
}
