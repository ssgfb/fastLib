package com.fastlib.demo.widget;

import com.fastlib.annotation.Bind;
import com.fastlib.annotation.ContentView;
import com.fastlib.demo.R;
import com.fastlib.utils.N;
import com.fastlib.widget.BottomDialog;

/**
 * Created by sgfb on 2020\03\09.
 */
@ContentView(R.layout.dialog_demo)
public class BottomDialogDemo extends BottomDialog{

    @Override
    protected void bindView() {
        N.showShort(getContext(),"dialog已生成");
    }

    @Bind(R.id.test1)
    private void test1Click(){
        N.showShort(getContext(),"点击1");
    }

    @Bind(R.id.test2)
    private void test2Click(){
        N.showShort(getContext(),"点击2");
        dismiss();
    }
}
