package com.fastlib.demo.widget;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.fastlib.demo.R;
import com.fastlib.widget.DoubleSeekBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by sgfb on 2020\03\09.
 * 自定义View例子
 */
public class DoubleProgressDemoActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_double_progress_demo);

        final DoubleSeekBar doubleSeekBar =findViewById(R.id.doubleProgress);
        doubleSeekBar.setMin(0);
        doubleSeekBar.setMax(100);
        doubleSeekBar.setOnProgressChangedListener(new DoubleSeekBar.OnProgressChanged() {
            @Override
            public void progressChanged(boolean fromUser, int leftValue, int rightValue) {
                System.out.println("from user:"+fromUser+" leftValue:"+leftValue+" rightValue:"+rightValue);
            }
        });

        findViewById(R.id.changeLeftCircleColor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doubleSeekBar.setLeftCircleColor(Color.parseColor("#FF0000"));
            }
        });
        findViewById(R.id.changeRightCircleColor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doubleSeekBar.setRightCircleColor(Color.parseColor("#FFFF00"));
            }
        });
        findViewById(R.id.changeBackgroundColor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doubleSeekBar.setBackgroundColor(Color.parseColor("#0000FF"));
            }
        });
        findViewById(R.id.changeProgressColor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doubleSeekBar.setProgressColor(Color.parseColor("#00FFFF"));
            }
        });
    }
}
