package com.fastlib.demo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.fastlib.widget.PinchImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by sgfb on 2020\04\10.
 */
public class SinglePointerScalableViewGroup extends FrameLayout {
    private boolean mHoldScalePointer;
    private float mOldX;
    private float mOldY;
    private int mOldWidth;
    private int mOldHeight;

    public SinglePointerScalableViewGroup(@NonNull Context context) {
        super(context);
    }

    public SinglePointerScalableViewGroup(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        float x=ev.getX();
        float y=ev.getY();

        if(ev.getAction()==MotionEvent.ACTION_DOWN){
            if(x>getWidth()-20&&x<getWidth()&&y>getHeight()-20&&y<getHeight()) {
                System.out.println("命中缩放");
                return true;
            }
            else if(x<20||y<20){
                System.out.println("命中平移");
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x=event.getX();
        float y=event.getY();

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                if(x>getWidth()-20&&x<getWidth()&&y>getHeight()-20&&y<getHeight()) {
                    mHoldScalePointer = true;
                    mOldX=x;
                    mOldY=y;
                    mOldWidth =getWidth();
                    mOldHeight =getHeight();
                    return true;
                }
            case MotionEvent.ACTION_MOVE:
                float diffX=x-mOldX;
                float diffY=y-mOldY;
                if(mHoldScalePointer){
                    ViewGroup.LayoutParams lp=getLayoutParams();
                    lp.width= (int) (mOldWidth+diffX);
                    lp.height= (int) (mOldHeight+diffY);
                    setLayoutParams(lp);
                }
                else{
                    offsetLeftAndRight((int) diffX);
                    offsetTopAndBottom((int) diffY);
                }
                return true;
            case MotionEvent.ACTION_UP:
                mHoldScalePointer=false;
                break;
        }
        return false;
    }
}
