package com.fastlib.demo.widget;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.fastlib.annotation.ContentView;
import com.fastlib.demo.MainActivity;
import com.fastlib.demo.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by sgfb on 2020\03\09.
 */
public class WidgetDemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.act_widget_demo);
        findViewById(R.id.doubleProgress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WidgetDemoActivity.this,DoubleProgressDemoActivity.class));
            }
        });
        findViewById(R.id.scalableView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WidgetDemoActivity.this,ScalableViewGroupActivity.class));
            }
        });
        findViewById(R.id.showBottomDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new BottomDialogDemo().show(getSupportFragmentManager());
            }
        });
        findViewById(R.id.singlePointerScale).setOnClickListener(v->{
            startActivity(new Intent(WidgetDemoActivity.this,SinglePointerScalableViewActivity.class));
        });
        findViewById(R.id.replaceable).setOnClickListener(v->{
            startActivity(new Intent(WidgetDemoActivity.this,ReplaceableLayoutActivity.class));
        });
    }
}
