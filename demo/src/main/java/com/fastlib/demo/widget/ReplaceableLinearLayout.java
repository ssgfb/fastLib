package com.fastlib.demo.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.fastlib.utils.ScreenUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.ViewUtils;
import androidx.core.view.ViewCompat;
import androidx.customview.widget.ViewDragHelper;

/**
 * Created by sgfb on 2020\04\10.
 */
public class ReplaceableLinearLayout extends LinearLayout  {
    private ViewDragHelper mViewDragHelper;
    private ViewDragHelper.Callback mCallback=new ViewDragHelper.Callback() {
        private int mReplaceIndex;

        @Override
        public boolean tryCaptureView(@NonNull View child, int pointerId){
            for(int i=0;i<getChildCount();i++){
                if(getChildAt(i)==child){
                    mReplaceIndex=i;
                    break;
                }
            }
            child.setZ(10);
            return true;
        }

        @Override
        public int clampViewPositionHorizontal(@NonNull View child, int left, int dx){
            int[] location=new int[2];
            child.getLocationOnScreen(location);

            if(location[0]+child.getWidth()> ScreenUtils.getScreenWidth()){
                HorizontalScrollView horizontalScrollView= (HorizontalScrollView) getParent();
                horizontalScrollView.smoothScrollBy(child.getWidth(),0);
            }
            else if(location[0]<0){
                HorizontalScrollView horizontalScrollView= (HorizontalScrollView) getParent();
                horizontalScrollView.smoothScrollBy(-child.getWidth(),0);
            }

            boolean front=true;
            for(int i=0;i<getChildCount();i++){
                View view=getChildAt(i);
                if(view==child){
                    front=false;
                    continue;
                }
                if(front){
                    if(child.getLeft()<view.getLeft()){
                        removeView(view);
                        addView(view,i+1);
                        mReplaceIndex=i;
                        break;
                    }
                }
                else{
                    if(child.getRight()>view.getRight()) {
                        removeView(view);
                        addView(view,i-1);
                        mReplaceIndex=i;
                        break;
                    }
                }
            }
            return left;
        }

        @Override
        public int clampViewPositionVertical(@NonNull View child, int top, int dy) {
            return top;
        }

        @Override
        public void onViewReleased(@NonNull View releasedChild, float xvel, float yvel) {
            super.onViewReleased(releasedChild, xvel, yvel);
            releasedChild.setZ(0);
            removeView(releasedChild);
            addView(releasedChild,mReplaceIndex);
        }
    };

    public ReplaceableLinearLayout(Context context) {
        super(context);
        mViewDragHelper=ViewDragHelper.create(this,mCallback);
    }

    public ReplaceableLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mViewDragHelper=ViewDragHelper.create(this,mCallback);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mViewDragHelper.shouldInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mViewDragHelper.processTouchEvent(event);
        return true;
    }

    @Override
    public void computeScroll() {
        if(mViewDragHelper.continueSettling(true))
            ViewCompat.postInvalidateOnAnimation(this);
    }
}
