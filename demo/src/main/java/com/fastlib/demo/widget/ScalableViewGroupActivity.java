package com.fastlib.demo.widget;

import android.os.Bundle;

import com.fastlib.demo.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by sgfb on 2020\03\09.
 * 可缩放父视图测试
 */
public class ScalableViewGroupActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_scalable_demo);
    }
}
