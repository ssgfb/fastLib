package com.fastlib.demo.app;

import android.app.Application;
import androidx.recyclerview.widget.RecyclerView;
import com.fastlib.demo.list_view.RemoteBindAdapterDemo;
import com.fastlib.demo.net.TestInterface;
import com.fastlib.net.utils.RequestAgentFactory;
import com.fastlib.utils.ContextHolder;
import com.fastlib.utils.fitout.AttachmentFitout;
import com.fastlib.utils.fitout.FitoutFactory;
import com.fastlib.utils.fitout.InstanceMaker;

/**
 * Created by sgfb on 2020\03\02.
 */
public class AppApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ContextHolder.init(this);
//        Hotfix.launcher();
//        RuntimeDataServerHandle.registerRuntimeDataCallback("memory", new InflexibleRuntimeDataCallback() {
//            @Override
//            public String showData() {
//                Runtime runtime=Runtime.getRuntime();
//                return String.format(Locale.getDefault(),"%s/%s max:%s",
//                        Formatter.formatFileSize(AppApplication.this,runtime.totalMemory()-runtime.freeMemory()),
//                        Formatter.formatFileSize(AppApplication.this,runtime.totalMemory()),
//                        Formatter.formatFileSize(AppApplication.this,runtime.maxMemory()));
//            }
//        });
//        AspectManager.getInstance().init(this);
//        AspectManager.getInstance().addStaticEnv(CustomerPermissionHandler.class);
//        CustomViewInject.inflaterCustomViewInject();
//        initCustomFitout();

//        startService(new Intent(this, BackendBridgeService.class).putExtra(BackendBridgeService.ARG_INT_COMMAND,BackendBridgeService.COMMAND_START));
    }

    private void initCustomFitout(){
        FitoutFactory.getInstance().putInstanceMaker(new InstanceMaker() {
            @Override
            public Object makeInstance(Class cla) {
                return RequestAgentFactory.genAgent(cla);
            }
        },TestInterface.class);
        FitoutFactory.getInstance().putAttachmentFitout(new AttachmentFitout() {
            @Override
            public void fitout(Object fieldInstance, Object attachment) {
                ((RecyclerView)fieldInstance).setAdapter((RecyclerView.Adapter) attachment);
            }
        },RemoteBindAdapterDemo.class);
    }
}
