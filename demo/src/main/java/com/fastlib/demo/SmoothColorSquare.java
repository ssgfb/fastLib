package com.fastlib.demo;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class SmoothColorSquare extends Square{
    private float[] colors={
            1f,0f,0f,1f,
            0f,1f,0f,1f,
            0f,0f,1f,1f,
            1f,0f,1f,1f
    };

    @Override
    public void draw(GL10 gl) {
        ByteBuffer cbb=ByteBuffer.allocateDirect(colors.length*4);
        cbb.order(ByteOrder.nativeOrder());

        FloatBuffer fb=cbb.asFloatBuffer();
        fb.put(colors);
        fb.position(0);
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,vertexBuffer);
        gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
        gl.glColorPointer(4, GL10.GL_FLOAT,0,fb);
        super.draw(gl);
        gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
    }
}
