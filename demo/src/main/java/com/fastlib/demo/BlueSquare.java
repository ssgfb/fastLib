package com.fastlib.demo;

import javax.microedition.khronos.opengles.GL10;

public class BlueSquare extends Square{

    @Override
    public void draw(GL10 gl) {
        gl.glColor4f(0.5f,0.5f,1f,1.0f);
        super.draw(gl);
    }
}
