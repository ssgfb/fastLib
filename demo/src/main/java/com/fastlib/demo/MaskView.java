package com.fastlib.demo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author sgfb
 * @Date 2020/8/19 20:21
 * <p>
 * 最小点应该是leftTop+1，最大点是rightBottom—1.保证必定有两点交接
 **/
public class MaskView extends View {
    private float centerX;
    private float centerY;
    private int angle;
    private Paint paint = new Paint();
    private Paint circlePaint = new Paint();
    private Path path;

    public MaskView(Context context) {
        super(context);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(3);
        paint.setAntiAlias(true);
        path = new Path();

        circlePaint.setColor(Color.RED);
        circlePaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float startX = 0;
        float endX = getWidth();
        float startY;
        float endY;

        int degree = angle % 360;
        boolean degreeBigger180=angle%360>=180;
        boolean degreeOpposite=degree > 90 && degree < 270;
        if (centerX == 0)
            centerX = getWidth() / 2f;
        if (centerY == 0)
            centerY = getHeight() / 2f;

        //直角不存在正切，特殊处理
        if (degree == 90) {
            startX = (int) centerX;
            startY = 0;
            endX = (int) centerX;
            endY = getHeight();
        }
        else if(degree==270){
            startX= (int) centerX;
            startY= getHeight();
            endX= (int) centerX;
            endY= 0;
        }
        else {
            //根据中心位置和角度计算两个焦点.每90度反转一次，90特殊角度直接取图片高度
            //用正切推出对边高，计算得到两个点的Y值
            double widthWithoutCenterX=getWidth()-centerX;
            double startBaseLineLength;
            double endBaseLineLength;
            if(degree<90){
                startBaseLineLength=centerX;
                endBaseLineLength=widthWithoutCenterX;
            }
            else if(degree<180){
                startBaseLineLength=widthWithoutCenterX;
                endBaseLineLength=widthWithoutCenterX;
                double tempLength=Math.abs(Math.tan(Math.toRadians(angle + 180)) * endBaseLineLength);
                if(centerY - tempLength*(degreeBigger180?-1:1)>0)
                    endBaseLineLength=centerX;
            }
            else if(degree<270){
                startBaseLineLength=centerX;
                endBaseLineLength=centerX;
            }
            else{
                startBaseLineLength=widthWithoutCenterX;
                endBaseLineLength=widthWithoutCenterX;
            }

            double startYLength = Math.abs(Math.tan(Math.toRadians(angle))) * startBaseLineLength;
            double endYLength = Math.abs(Math.tan(Math.toRadians(angle + 180)) * endBaseLineLength);
            startY = (int) (centerY - startYLength*(degreeBigger180?-1:1));
            endY = (int) (centerY + endYLength*(degreeBigger180?-1:1));

//            System.out.println(endY);
            //如果Y点超出左右两边，则计算落在上下两边的点
            if(startY<0){
                if(degreeOpposite)
                    startX=getWidth()-(int) (Math.abs(startY)*Math.tan(Math.toRadians(angle-90)));
                else
                    startX= (int) (Math.abs(startY)*Math.tan(Math.toRadians(90- angle)));
                startY=0;
            }
            else if(startY>getHeight()){
                if(degreeOpposite)
                    startX=(int) (Math.abs(startY)*Math.tan(Math.toRadians(90- angle)));
                else
                    startX= (int) (getWidth()+(Math.abs(startY)*Math.tan(Math.toRadians(90- angle))));
                startY=getHeight();
            }
            if(endY<0){
                if(degreeOpposite)
                    endX=(int) (Math.abs(endY)*Math.tan(Math.toRadians(270- angle)));
                else
                    endX= getWidth()+(int) (Math.abs(endY)*Math.tan(Math.toRadians(270-Math.abs(angle))));
                endY=0;
            }
            else if(endY>getHeight()) {
                if(degreeOpposite){
                    endX= (float) (getWidth()-Math.abs(startY * Math.tan(Math.toRadians(270-angle))));
                    System.out.println(endX);
                }
                else
                    endX = getWidth()-(int) (Math.abs(endY-getHeight()) * Math.tan(Math.toRadians(270 - angle)));
                endY = getHeight();
            }
        }

        //如果角度落在90~270之间，左右Y轴的点换线
        if (degreeOpposite) {
            if (startX == 0)
                startX = getWidth();
            if (endX == getWidth())
                endX = 0;
        }

        //计算逆时针路线得到一个闭合形状
        List<Point> list = new ArrayList<>();
        if (startX == 0) {
            list.add(new Point(0, getHeight()));
            if (endY < getHeight())
                list.add(new Point(getWidth(), getHeight()));
            if (endY == 0 && endX < getWidth())
                list.add(new Point(getWidth(), 0));
        }
        if (startY == getHeight()) {
            list.add(new Point(getWidth(), getHeight()));
            if (endY == 0)
                list.add(new Point(getWidth(), 0));
            if (endY > 0)
                list.add(new Point());
        }
        if (startX == getWidth()) {
            list.add(new Point(getWidth(), 0));
            if (endY > 0)
                list.add(new Point());
            if (endY == getHeight())
                list.add(new Point(0, getHeight()));
        }
        if (startY == 0) {
            list.add(new Point());
            list.add(new Point(0, getHeight()));
            if (endX == getWidth())
                list.add(new Point(getWidth(), getHeight()));
        }

        path.reset();
        path.moveTo(startX, startY);
        for (Point point : list)
            path.lineTo(point.x, point.y);
        path.lineTo(endX, endY);
        path.close();
//        canvas.drawPath(path,paint);

        paint.setColor(Color.WHITE);
        canvas.drawLine(startX, startY, endX, endY, paint);
        paint.setColor(Color.BLUE);
        canvas.drawCircle(startX, startY, 10, paint);
        paint.setColor(Color.RED);
        canvas.drawCircle(endX, endY, 10, paint);

        canvas.drawCircle(centerX,getHeight()/2f,10,circlePaint);
    }

    public void changeSplitLineVertical(int value) {
        centerX = getWidth() / 2f + value;
        invalidate();
    }

    public void changeSplitLineHorizontal(int value) {
        invalidate();
    }

    public void changeAngle(int angle) {
        this.angle = angle;
        invalidate();
    }
}