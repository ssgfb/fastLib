package com.fastlib.demo;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tencent.smtt.sdk.WebSettings;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(JUnit4.class)
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() {
        WebSettings w;
        JsonObject jo=new JsonParser().parse("{\"code\":400231,\"msg\":\"资料审核中，暂不能回复问题\",\"data\":{\"a\":1}}").getAsJsonObject();
        JsonElement data=jo.get("data");
        if(data.isJsonObject()){
            System.out.println(jo.toString());
        }
    }
}